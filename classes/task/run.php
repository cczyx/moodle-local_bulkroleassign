<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\task;

use local_bulkroleassign\local\rule;

/**
 * Adhoc task to run a single rule.
 *
 * This task is intended for use whenever a rule is updated so
 * that the user list can be corrected as soon as possible.
 * 
 * The task requires that the following custom data parameters:
 * - ruleid: The id of the rule to be run.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class run extends \core\task\adhoc_task {
    /**
     * Runs the task.
     *
     * @return void
     */
    public function execute() {
        // Force the custom data to be an object.
        $data = (object)$this->get_custom_data();
        if (!isset($data->ruleid)) {
            mtrace('Invalid task, no ruleid set.');
            // The rule we are to execute has not been passed.
            return;
        }
        try {
            $rule = new rule($data->ruleid);
            mtrace("Running rule $data->ruleid", " ");
            $rule->run();
            $success = true;
            mtrace("...success.");
        } catch (\local_bulkroleassign\local\invalid_rule $e) {
            mtrace("...rule invalid.");
        }
    }
}
