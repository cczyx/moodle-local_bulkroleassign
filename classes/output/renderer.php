<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace local_bulkroleassign\output;

use \local_bulkroleassign\local\rule;

defined('MOODLE_INTERNAL') || die();

/**
 * Description of renderer
 *
 * @author mzznm1
 */
class renderer extends \plugin_renderer_base {
    /**
     * Renders an index object.
     *
     * @param \local_bulkroleassign\local\index $index
     * @return string
     */
    public function render_index(index $index) {
        $data = $index->export_for_template($this);
        return parent::render_from_template('local_bulkroleassign/index', $data);
    }

    /**
     * Renders a preview object.
     *
     * @param \local_bulkroleassign\output\preview $preview
     * @return string
     */
    public function render_preview(preview $preview) {
        $data = $preview->export_for_template($this);
        return parent::render_from_template('local_bulkroleassign/preview', $data);
    }
}
