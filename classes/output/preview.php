<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\output;

/**
 * Renderable for the preview page
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class preview implements \renderable, \templatable {
    /** The number of users to be displayed on a page. */
    const USERCOUNT = 50;

    /** @var \html_table Table of users that are found by the rule. */
    protected $usertable;
    /** @var \paging_bar The paging bar for the preview page. */
    protected $paging;
    /** @var \html_table Table containing a summary of the rule. */
    protected $ruledetails;
    /** @var bool Class to indicate if the rule is valid or not. */
    protected $valid;

    /**
     * Constructor.
     *
     * @param \local_bulkroleassign\local\rule $rule The rule to be previewed.
     * @param \moodle_url $url The url of the page.
     * @param int $page the page of users to be displayed.
     */
    public function __construct(\local_bulkroleassign\local\rule $rule, \moodle_url $url, $page) {
        $totalusers = $rule->count_users();
        // Build a table containing details about the rule.
        $this->ruledetails = new \html_table();
        $this->ruledetails->head = array(
            get_string('ruletitle', 'local_bulkroleassign'),
            get_string('ruledescription', 'local_bulkroleassign'),
            get_string('usercount', 'local_bulkroleassign'),
        );
        $this->valid = $rule->is_valid();
        $details = array(
            $rule->title,
            $rule->description,
            $totalusers,
        );
        $this->ruledetails->data = array(
            new \html_table_row($details),
        );
        // A table containing the users found by the rule for this page.
        $this->usertable = new \html_table();
        $this->usertable->head = array(
            get_string('user'),
            get_string('idnumber'),
            get_string('email'),
        );
        $users = array();
        $userlist = $rule->get_user_list(self::USERCOUNT, $page);
        foreach ($userlist as $user) {
            $userrow = array(
                fullname($user),
                $user->idnumber,
                $user->email,
            );
            $users[] = new \html_table_row($userrow);
        }
        $this->usertable->data = $users;
        // Paging bar.
        $this->paging = new \paging_bar($totalusers, $page, self::USERCOUNT, $url);
    }

    /**
     * @see \templatable::export_for_template
     */
    public function export_for_template(\renderer_base $output) {
        $export = new \stdClass();
        if (!$this->valid) {
            $export->invalidmessage = $output->notification(get_string('invalidrule', 'local_bulkroleassign'), 'error');
        }
        $export->ruledetails = \html_writer::table($this->ruledetails);
        $export->paging = $output->render($this->paging);
        $export->usertable = \html_writer::table($this->usertable);
        return $export;
    }
}
