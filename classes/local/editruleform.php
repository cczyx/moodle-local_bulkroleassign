<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->libdir/formslib.php");

/**
 * Form for editing a rule.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class editruleform  extends ruleform {
    /** @var \local_bulkroleassign\local\rule The rule object that is being edited. */
    protected $rule;

    /** @var \local_bulkroleassign\local\filter_user[] An array of default core user filters. */
    protected $corefilters = array();

    /** @var \local_bulkroleassign\local\filter_user_info_data[] An array of default custom profile filters. */
    protected $customfilters = array();

    /**
     * Creates the fields for the edit form.
     *
     * @throws \coding_exception
     */
    public function definition() {
        $this->rule = new rule($this->_customdata['rule']);
        // Store the filters in the best way we can.
        foreach ($this->rule->filters as $filter) {
            switch ($filter->type) {
                case 'user':
                    $this->corefilters[] = $filter;
                    break;
                case 'user_info_data':
                    $this->customfilters[] = $filter;
                    break;
                default:
                    throw new \coding_exception('Uknown filter type.');
            }
        }
        // The form is mostly the same as the add form.
        parent::definition();
    }

    /**
     * @see \local_bulkroleassign\local\ruleform::add_rule_elements
     */
    protected function add_rule_elements() {
        parent::add_rule_elements();
        $mform = $this->_form;
        $mform->setDefault('title', $this->rule->title);
        $mform->setDefault('description', $this->rule->description);
        $mform->setDefault('roleid', $this->rule->role);
        $mform->setDefault('categoryid', $this->rule->context->instanceid);
        // Add the id of the rule to the form.
        $mform->addElement('hidden', 'id', $this->rule->id);
        $mform->setType('id', PARAM_INT);
    }

    /**
     * @see \local_bulkroleassign\local\ruleform::get_core_profile_filter
     */
    protected function get_core_profile_filter($prefix, $number) {
        $mform = $this->_form;
        $elements = parent::get_core_profile_filter($prefix, $number);
        $defaultid = 0;
        // Test if we should set some defaults.
        if (isset($this->corefilters[$number - 1])) {
            $filter = $this->corefilters[$number - 1];
            $defaultid = $filter->id;
            $mform->setDefault("{$prefix}field{$number}", $filter->field);
            $mform->setDefault("{$prefix}method{$number}", $filter->method);
            $mform->setDefault("{$prefix}value{$number}", $filter->value);
        }
        // Add a field to store the id of the filter.
        $elements[] = &$mform->createElement('hidden', "{$prefix}id{$number}", $defaultid);
        $mform->setType("{$prefix}id{$number}", PARAM_INT);
        return $elements;
    }

    /**
     * @see \local_bulkroleassign\local\ruleform::get_custom_profile_filter
     */
    protected function get_custom_profile_filter($prefix, $number) {
        $mform = $this->_form;
        $elements = parent::get_custom_profile_filter($prefix, $number);
        $defaultid = 0;
        // Test if we should set some defaults.
        if (isset($this->customfilters[$number - 1])) {
            $filter = $this->customfilters[$number - 1];
            $defaultid = $filter->id;
            $mform->setDefault("{$prefix}field{$number}", $filter->field);
            $mform->setDefault("{$prefix}method{$number}", $filter->method);
            $mform->setDefault("{$prefix}value{$number}", $filter->value);
        }
        // Add a field to store the id of the filter.
        $elements[] = &$mform->createElement('hidden', "{$prefix}id{$number}", $defaultid);
        $mform->setType("{$prefix}id{$number}", PARAM_INT);
        return $elements;
    }

    /**
     * Updates a rule and its filters based on the form data.
     */
    public function save() {
        $data = $this->get_data();
        $rule = $this->rule;
        $rule->title = $data->title;
        $rule->description = $data->description;
        $rule->role = $data->roleid;
        $rule->context = \context_coursecat::instance($data->categoryid);

        for ($i = 1; $i <= self::MAXFILTERS; $i++) {
            // Core user fields.
            if (!empty($data->{"corefilterid$i"}) && !empty($data->{"corefiltervalue$i"})) {
                // Existing filter.
                $this->edit_core_filter($i, $data);
            } else if (!empty($data->{"corefiltervalue$i"})) {
                // It is a new filter.
                $rule->add_filter($this->create_core_filter($rule, $i, $data));
            } else if (!empty($this->corefilters[$i -1])) {
                // The filter has been deleted.
                $this->corefilters[$i -1]->mark_for_delete();
            }
            // Custom user profile fields.
            if (!empty($data->{"customfilterid$i"}) && !empty($data->{"customfiltervalue$i"})) {
                // Existing filter.
                $this->edit_custom_filter($i, $data);
            } else if (!empty($data->{"customfiltervalue$i"})) {
                // It is a new filter.
                $rule->add_filter($this->create_custom_filter($rule, $i, $data));
            } else if (!empty($this->customfilters[$i - 1])) {
                // The filter has been deleted.
                $this->customfilters[$i - 1]->mark_for_delete();
            }
        }
        $rule->save();
        // Set a task that will run the rule to update the assignments.
        $task = new \local_bulkroleassign\task\run();
        $task->set_custom_data(array('ruleid' => $rule->id));
        \core\task\manager::queue_adhoc_task($task);
    }

    /**
     * Edits the values of the specified user profile field filter.
     *
     * @param int $number
     * @param stdClass $data The form data.
     * @return void
     */
    protected function edit_core_filter($number, $data) {
        $filter = &$this->corefilters[$number - 1];
        $filter->field = $data->{"corefilterfield$number"};
        $filter->value = $data->{"corefiltervalue$number"};
        $filter->method = $data->{"corefiltermethod$number"};
    }

    /**
     * Edits the values of the specified custom profile field filter.
     *
     * @param int $number
     * @param stdClass $data The form data.
     * @return void
     */
    protected function edit_custom_filter($number, $data) {
        $filter = &$this->customfilters[$number - 1];
        $filter->field = $data->{"customfilterfield$number"};
        $filter->value = $data->{"customfiltervalue$number"};
        $filter->method = $data->{"customfiltermethod$number"};
    }
}
