<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

defined('MOODLE_INTERNAL') || die();

/**
 * Abstract class that should be extended for all filter
 * types that can be used for selecting users for bulk role
 * assign rules.
 *
 * The main purpose of these classes is to generate the SQL
 * for the filter in a hopefully easy to understand way.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
abstract class filter {
    /** The filter matched the string exactly. */
    const METHOD_EQUALS = 1;
    /** The filter looks for values with the string in them. */
    const METHOD_CONTAINS = 2;
    /** The filter looks for values starting with the string. */
    const METHOD_BEGINS = 3;
    /** The filter looks for values ending with the string. */
    const METHOD_ENDS = 4;

    /** The filter will match data from the user table. */
    const TYPE_CORE = 'user';
    /** The filter will match Custom profile fields. */
    const TYPE_CUSTOM = 'user_info_data';

    /** @var bool A flag to mark the filter for deletion during a save. */
    protected $delete = false;
    /** @var string The Moodle user field that the filter is for. */
    public $field;
    /** @var int The id of the filter. */
    protected $id;
    /** @var int The method of searching. */
    public $method;
    /** @var \local_bulkroleassign\local\rule the rule the filter is part of. */
    protected $rule;
    /** @var string The type of profile field being filtered. */
    protected $type;
    /** @var string The value to search for. */
    public $value;

    /**
     * Constructs the filter.
     *
     * @param \local_bulkroleassign\local\rule $rule
     * @param string $field
     * @param string $value
     * @param type $method
     * @param int $id
     */
    public function __construct(rule $rule, $field, $value, $method, $id = null) {
        if (!is_null($id)) {
            $this->id = clean_param($id, PARAM_INT);
        }
        $this->field = $field;
        $this->method = $method;
        $this->rule = $rule;
        $this->value = $value;
    }

    /**
     * Access protected properties.
     *
     * @param type $name
     * @return type
     * @throws \coding_exception
     */
    public function __get($name) {
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        throw new \coding_exception('Trying to access undefined property');
    }

    /**
     * Clones the filter record.
     */
    public function clone_filter() {
        $this->create();
    }

    /**
     * Create the filter in as a new record in the database.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return void
     */
    protected function create() {
        global $DB;
        $record = (object)array(
            'ruleid' => $this->rule->id,
            'fldtype' => $this->type,
            'userfld' => $this->field,
            'filter_value' => $this->value,
            'method' => $this->method,
        );
        $this->id = $DB->insert_record('local_bulkroleassign_ufilter', $record);
    }

    protected function delete() {
        global $DB;
        if (!isset($this->id)) {
            // Not saved to the database yet.
            return;
        }
        $DB->delete_records('local_bulkroleassign_ufilter', array('id' => $this->id));
    }

    /**
     * Gets the alias for the table that the filter will query.
     *
     * @return string
     */
    protected function get_alias() {
        // Will return a value like: f1 or f69.
        return "f$this->id";
    }

    /**
     * Loads all the filters for a given rule.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @param \local_bulkroleassign\local\rule $rule
     * @return \local_bulkroleassign\local\filter[]
     */
    public static function get_filters(rule $rule) {
        global $DB;
        $filters = array();
        // Load all the filters for a rule.
        $rawfilters = $DB->get_records('local_bulkroleassign_ufilter', array('ruleid' => $rule->id));
        foreach($rawfilters as $rawfilter) {
            $filterclass = '\local_bulkroleassign\local\filter_' . $rawfilter->fldtype;
            $field = $rawfilter->userfld;
            $value = $rawfilter->filter_value;
            $method = $rawfilter->method;
            $id = $rawfilter->id;
            $filter = new $filterclass($rule, $field, $value, $method, $id);
            // Assign to the return array.
            $filters[$id] = $filter;
        }
        return $filters;
    }

    /**
     * Returns a list of filter methods that filters support.
     *
     * The key is the identifier for the method, the value a localised name.
     *
     * @return arry
     */
    public static function get_method_select() {
        return array(
            self::METHOD_EQUALS => get_string('method_equals', 'local_bulkroleassign'),
            self::METHOD_CONTAINS => get_string('method_contains', 'local_bulkroleassign'),
            self::METHOD_BEGINS => get_string('method_begins', 'local_bulkroleassign'),
            self::METHOD_ENDS => get_string('method_ends', 'local_bulkroleassign'),
        );
    }

    /**
     * Mark the filter for deletion.
     *
     * @return void
     */
    public function mark_for_delete() {
        $this->delete = true;
    }

    /**
     * Test that the filter is valid.
     *
     * @return bool true if the filter is valid, otherwise false.
     */
    public function is_valid() {
        // Check the type is valid.
        $classname = '\local_bulkroleassign\local\filter_' . $this->type;
        $validfield = array_key_exists($this->field, $classname::get_valid_types());
        // Check the method is valid.
        $validmethod = array_key_exists($this->method, self::get_method_select());
        return $validfield && $validmethod;
    }

    /**
     * Save the filter to the database.
     *
     * @return void
     */
    public function save() {
        if ($this->delete) {
            $this->delete();
        } else if (!isset($this->id)) {
            $this->create();
        } else {
            $this->update();
        }
    }

    /**
     * Get the SQL required to run the filter.
     *
     * @return array array contains five parameters:
     *      - The name of the table
     *      - The alias for the table
     *      - The ON statement for the table
     *      - the where SQL
     *      - the values of parameters used in the where clause
     */
    public function sql() {
        list($table, $on) = $this->sql_table_name();
        $alias = $this->get_alias();
        list($where, $params) = $this->sql_where();
        return array($table, $alias, $on, $where, $params);
    }
    
    /**
     * Get the where clause for the filter's SQL when it uses the begins with method.
     *
     * @return array contains two values:
     *      - the where SQL
     *      - the values of parameters used in the where clause
     */
    abstract protected function sql_begins();

    /**
     * Get the where clause for the filter's SQL when it uses the contains method.
     *
     * @return array contains two values:
     *      - the where SQL
     *      - the values of parameters used in the where clause
     */
    abstract protected function sql_contains();

    /**
     * Get the where clause for the filter's SQL when it uses the equals method.
     *
     * @return array contains two values:
     *      - the where SQL
     *      - the values of parameters used in the where clause
     */
    abstract protected function sql_equals();

    /**
     * Get the where clause for the filter's SQL when it uses the ends with method.
     *
     * @return array contains two values:
     *      - the where SQL
     *      - the values of parameters used in the where clause
     */
    abstract protected function sql_ends();

    /**
     * Get the name of the table used by the filter.
     *
     * @return array contains two parameters:
     *      - The name of the table
     *      - The ON statement for the table
     */
    abstract protected function sql_table_name();

    /**
     * Get the where clause for the filter's SQL.
     * 
     * This method routes the request to an appropriate sub method
     * for the method the filter uses. Sub-classes should override this
     * if they have parts of the where clause that do not change based on
     * the filters method.
     *
     * @return array contains two values:
     *      - the where SQL
     *      - the values of parameters used in the where clause
     */
    protected function sql_where() {
        switch ($this->method) {
            case self::METHOD_BEGINS:
                return $this->sql_begins();
            case self::METHOD_CONTAINS:
                return $this->sql_contains();
            case self::METHOD_EQUALS:
                return $this->sql_equals();
            case self::METHOD_ENDS:
                return $this->sql_ends();
        }
        throw new \coding_exception('Invalid or unimplememnted filter method used');
    }

    /**
     * Update the filter record in the database.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return void
     */
    protected function update() {
        global $DB;
        $record = (object)array(
            'id' => $this->id,
            'ruleid' => $this->rule->id,
            'fldtype' => $this->type,
            'userfld' => $this->field,
            'filter_value' => $this->value,
            'method' => $this->method,
        );
        $DB->update_record('local_bulkroleassign_ufilter', $record);
    }
}
