<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->libdir/formslib.php");

/**
 * Form for cloning a rule.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class cloneform extends \moodleform {
    /** @var \local_bulkroleassign\local\rule The rule object that is being edited. */
    protected $rule;

    /**
     * Setup the cloning form.
     */
    public function definition() {
        $mform = $this->_form;
        $this->rule = new rule($this->_customdata['rule']);

        // Add the id of the rule to the form.
        $mform->addElement('hidden', 'id', $this->rule->id);
        $mform->setType('id', PARAM_INT);
        // Cloning requires that a new title is provided.
        $mform->addElement('text', 'title', get_string('ruletitle', 'local_bulkroleassign'));
        $mform->setType('title', PARAM_TEXT);
        $mform->addRule('title', get_string('required'), 'required');
        $mform->addHelpButton('title', 'ruletitle', 'local_bulkroleassign');

        $buttonarray = array();
        $buttonarray[] = &$mform->createElement('submit', 'clone', get_string('clone', 'local_bulkroleassign'));
        $buttonarray[] = &$mform->createElement('cancel');
        $mform->addGroup($buttonarray, 'buttonar', '', array(' '), false);
        $mform->setType('buttonar', PARAM_RAW);
        $mform->closeHeaderBefore('buttonar');
    }

    /**
     * Perform the clone.
     */
    public function save() {
        $data = $this->get_data();
        $rule = $this->rule;
        // Load all the data for the rule, including filters.
        $rule->load();
        $rule->title = $data->title;
        $rule->clone_rule();
    }
}
