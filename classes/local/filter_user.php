<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

defined('MOODLE_INTERNAL') || die();

/**
 * A filter for a core user field.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class filter_user extends filter implements filter_active {
    /**
     * @see \local_bulkroleassign\local\filter::__construct()
     */
    public function __construct(rule $rule, $field, $value, $method, $id = null) {
        parent::__construct($rule, $field, $value, $method, $id);
        $this->type = filter::TYPE_CORE;
    }

    /**
     * @see \local_bulkroleassign\local\filter_active::get_valid_types()
     */
    public static function get_valid_types() {
        return array(
            'auth' => get_string('authentication'),
            'city' => get_string('city'),
            'country' => get_string('country'),
            'department' => get_string('department'),
            'institution' => get_string('institution'),
        );
    }

    protected function get_alias() {
        return 'u';
    }

    /**
     * @see \local_bulkroleassign\local\filter_active
     */
    public static function reset() {
        // Nothing to reset.
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_begins()
     *
     * @global \moodle_database $DB
     */
    protected function sql_begins() {
        global $DB;
        $where = $DB->sql_like("u.$this->field", ":data$this->id");
        $params = array(
            "data$this->id" => "$this->value%",
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_contains()
     */
    protected function sql_contains() {
        global $DB;
        $where = $DB->sql_like("u.$this->field", ":data$this->id");
        $params = array(
            "data$this->id" => "%$this->value%",
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_equals()
     */
    protected function sql_equals() {
        $where = "u.$this->field = :data$this->id";
        $params = array(
            "data$this->id" => $this->value,
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_ends()
     */
    protected function sql_ends() {
        global $DB;
        $where = $DB->sql_like("u.$this->field", ":data$this->id");
        $params = array(
            "data$this->id" => "%$this->value",
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_table_name()
     */
    protected function sql_table_name() {
        $tablename = '{user}';
        // The user table will always be present and aliased as u in the rule.
        $on = "";
        return array($tablename, $on);
    }
}
