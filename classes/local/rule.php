<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

defined('MOODLE_INTERNAL') || die();

/**
 * A rule defines the rules that should be used to select users for
 * assignment to a category or course in Moodle.
 *
 * It will add and remove users as they meet the criteria.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class rule {
    /** @var \context The is of the context of the category or course the rule is for. */
    protected $context;
    /** @var string The description of the rule. */
    protected $description;
    /** @var filter[] An array of filters for the rule. */
    protected $filters = array();
    /** @var bool Stores if the filters have been loaded. */
    protected $filtersloaded = false;
    /** @var int The id of the rule. */
    protected $id;
    /** @var bool Stores if the rule still needs loading from the database. */
    protected $loaded = false;
    /** @var int The id of the role that this rule should assign users to. */
    protected $role;
    /** @var array An array of Moodle roles, stored here as a cache, to save repeated database calls. */
    protected static $roles;
    /** @var string The title of the rule. */
    protected $title;
    
    /**
     * Constructs a rule.
     *
     * @param int $id the id of the rule, or null if this is a new rule.
     */
    public function __construct($id = null) {
        if (!is_null($id)) {
            $this->id = clean_param($id, PARAM_INT);
        } else {
            // This is a new record so everthing is already loaded.
            $this->loaded = true;
            $this->filtersloaded = true;
        }
    }

    /**
     * Access protected properties, this allows us to do lazy loading.
     *
     * @param string $name
     * @return mixed
     * @throws \coding_exception
     */
    public function __get($name) {
        // The names of propeties we do not wish to call a load.
        $exludefromload = array(
            'id' => true,
            'loaded' => true,
            'filtersloaded' => true,
        );
        if (isset($exludefromload[$name])) {
            // Intentionally blank.
        } else if ($name === 'filters' && !$this->filtersloaded) {
            // Load all data, including the filters.
            $this->load();
        } else if (!$this->loaded) {
            // Load the non filter data.
            $this->load(false);
        }
        if (property_exists($this, $name)) {
            return $this->$name;
        }
        throw new \coding_exception('Trying to access undefined property');
    }

    /**
     * Control the setting of protected properties.
     *
     * @param string $name
     * @param mixed $value
     */
    public function __set($name, $value) {
        if (!method_exists($this, "set_$name")) {
           throw new \coding_exception("Trying to set invalid property, no set_$name() method exists.");
        } 
        $this->{"set_$name"}($value);
    }

    /**
     * Add a filter to the rule.
     *
     * @param \local_bulkroleassign\local\filter $filter
     */
    public function add_filter(filter $filter) {
        // Ensure all the plugins filters are loaded.
        $this->load();
        // Add the filter.
        $this->filters[] = $filter;
    }

    /**
     * Generates the SELECT and FROM/WHERE parts of the SQL as two separate fragments.
     *
     * It returns he two fragments and the parameters for the query as a list
     * with the elements in the following order:
     * - SELECT fragment
     * - FROM/WHERE fragment
     * - params
     *
     * @return array
     */
    protected function base_sql() {
        $this->load();
        $select = "u.*";
        $tables = array(
            'u' => array(
                'name' => '{user}',
                'on' => array(),
            ),
        );
        $where = array();
        $params = array();

        foreach ($this->filters as $filter) {
            if (!$filter->is_valid()) {
                // Do not include invalid filters.
                continue;
            }
            list($filtertable, $filteralias, $filteron, $filterwhere, $filterparams) = $filter->sql();
            // Ensure that each aliased table is only used a single time.
            if (!isset($tables[$filteralias])) {
                $tables[$filteralias] = array(
                    'name' => '',
                    'on' => array(),
                );
            }
            $tables[$filteralias]['name'] = $filtertable;
            if (!empty($filteron)) {
                // Store unique on filters.
                $tables[$filteralias]['on'][md5($filteron)] = $filteron;
            }
            // All wheres should be used.
            $where[] = $filterwhere;
            // All paramteres should be used.
            $params = array_merge($params, $filterparams);
        }

        // Build the SQL from the filter parts.
        $tableintermediatesql = array();
        foreach ($tables as $alias => $tabledetails) {
            $tablefragment = $tabledetails['name'] . " $alias";
            if (!empty($tabledetails['on'])) {
                $tablefragment .= ' ON ' . implode(' AND ', $tabledetails['on']);
            }
            $tableintermediatesql[] = $tablefragment;
        }
        $tablesql = implode(' JOIN ', $tableintermediatesql);
        $wheresql = implode(' AND ', $where);
        if ($wheresql == '') {
            // There are no valid filters so add an impossible where.
            $wheresql = 'u.id IS NULL';
        } else {
            $wheresql .= ' AND u.deleted = 0'; // Make sure user is not deleted.
        }
        $selectfragment = "SELECT $select";
        $bodyfragment = "FROM $tablesql WHERE $wheresql";
        return array($selectfragment, $bodyfragment, $params);
    }

    /**
     * Cache all the roles so that on pages with many rules we do
     * not need to repeatedly query the database.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     */
    protected static function cache_roles() {
        global $DB;
        if (!isset(self::$roles)) {
            $roles = get_roles_for_contextlevels(CONTEXT_COURSECAT);
            list($insql, $roleparams) = $DB->get_in_or_equal($roles, SQL_PARAMS_NAMED, 'id');
            $where = 'id '. $insql;
            self::$roles = $DB->get_records_select('role', $where, $roleparams);
            role_fix_names(self::$roles, \context_system::instance(), ROLENAME_ORIGINAL);
        }
    }

    /**
     * Clones the rule.
     */
    public function clone_rule() {
        $this->load();
        $this->create();
        foreach ($this->filters as $filter) {
            $filter->clone_filter();
        }
    }

    /**
     * Returns the number of users affected by the rule.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return int
     */
    public function count_users() {
        global $DB;
        list($sql, $params) = $this->user_count_sql();
        return $DB->count_records_sql($sql, $params);
    }

    /**
     * Creates a new record based on this rule.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return void
     */
    protected function create() {
        global $DB;
        $rulerecord = (object)array(
            'rule_name' => $this->title,
            'rule_desc' => $this->description,
            'roleid' => $this->role,
            'contextid' => $this->context->id,
        );
        $this->id = $DB->insert_record('local_bulkroleassign_rules', $rulerecord);
    }

    /**
     * Delete the rule.
     *
     * @global \moodle_database $DB
     */
    public function delete() {
        global $DB;
        $this->delete_filters();
        // Delete any assignments.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->id,
        );
        $DB->delete_records('role_assignments', $params);
        // Delete the rule record.
        $DB->delete_records('local_bulkroleassign_rules', array('id' => $this->id));
        // Unset the if of the rule.
        $this->id = null;
    }

    /**
     * Delete the filters for the rule.
     *
     * @global \moodle_database $DB
     */
    public function delete_filters() {
        global $DB;
        $DB->delete_records('local_bulkroleassign_ufilter', array('ruleid' => $this->id));
        $this->filters = array();
    }

    /**
     * Deletes all rules for a category.
     * This method will be called just before Moodle deletes a category.
     * 
     * @global \moodle_database $DB The Moodle database connection object
     * @param int $categoryid
     */
    public static function delete_rules_for_category($categoryid) {
        global $DB;
        $context = \context_coursecat::instance($categoryid);
        $params = array('contextid' => $context->id);
        $ruleids = $DB->get_fieldset_select('local_bulkroleassign_rules', 'id', 'contextid = :contextid', $params);
        foreach ($ruleids as $ruleid) {
            $rule = new rule($ruleid);
            // Delete the rules filters.
            $rule->delete_filters();
        }
        // Delete all the assignments by this plugin on the category.
        $assignparams = array(
            'component' => 'local_bulkroleassign',
            'contextid' => $context->id,
        );
        $DB->delete_records('role_assignments', $assignparams);
        // Delete all the rule records in one go.
        $DB->delete_records('local_bulkroleassign_rules', array('contextid' => $context->id));
    }

    /**
     * Get all the role assign rules.
     *
     * @global \moodle_database $DB
     * @return \local_bulkroleassign\local\rule[]
     */
    public static function get_all_rules() {
        global $DB;
        $rawrules = $DB->get_records('local_bulkroleassign_rules');
        $rules = array();
        foreach ($rawrules as $rawrule) {
            $rule = new rule($rawrule->id);
            $rule->load(false, $rawrule);
            $rules[] = $rule;
        }
        return $rules;
    }

    /**
     * Gets an array of categories in a form that can be used in a select.
     *
     * @return array
     */
    public static function get_category_select() {
        return \coursecat::make_categories_list();
    }

    /**
     * Gets all the users who meet the rule criteria, but have not yet been assigned.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return \moolde_recordset A record set of user ids to be added.
     */
    public function get_new_users() {
        global $DB;
        list($sql, $params) = $this->user_new_sql();
        return $DB->get_recordset_sql($sql, $params);
    }

    /**
     * Gets all user who have been assigned by the rule, but no longer meet the criteria.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return \moodle_recordset A record set of role_assignment records to be removed.
     */
    public function get_old_users() {
        global $DB;
        list($sql, $params) = $this->user_old_sql();
        return $DB->get_recordset_sql($sql, $params);
    }

    /**
     * Get the name of the role.
     *
     * @return string
     */
    public function get_role_name() {
        self::cache_roles();
        if (!isset(self::$roles[$this->role])) {
            // The role on this rule is no longer assignable to a category.
            $name = get_string('invalidrole', 'local_bulkroleassign');
        } else {
            $name = self::$roles[$this->role]->localname;
        }
        return $name;
    }

    /**
     * Gets an array of role names that can be assigned to a rule,
     * in a form that can be used in a select.
     *
     * @return array
     */
    public static function get_role_select() {
        self::cache_roles();
        $roleselect = array();
        foreach (self::$roles as $role) {
            $roleselect[$role->id] = $role->localname;
        }
        return $roleselect;
    }

    /**
     * Returns a list of users that will be affected by this rule.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @param int $maxusers The maximum number of users to return
     * @param int $page The page of users to return (should start from 1)
     * @return array An array of user records from the database.
     */
    public function get_user_list($maxusers, $page = 0) {
        global $DB;
        if ($page < 0) {
            $page = 0;
        }
        list($sql, $params) = $this->user_select_sql();
        return $DB->get_records_sql($sql, $params, $page, $maxusers);
    }

    /**
     * Checks if the rule is valid.
     *
     * @return bool
     */
    public function is_valid() {
        $this->load(true);
        // Check there is a title and description.
        $valid = !empty($this->title) && !empty($this->description);
        // Check there is a context.
        $valid = $valid && $this->context instanceof \context;
        // Check the role is valid.
        $valid = $valid && array_key_exists($this->role, self::get_role_select());
        // There must be at least one filter.
        $valid = $valid && (count($this->filters) > 0);
        // Check that all the filters are valid.
        foreach ($this->filters as $filter) {
            // We will only check a filter if everything so far is valid.
            $valid = $valid && $filter->is_valid();
        }
        return $valid;
    }

    /**
     * Loads data for the rule.
     *
     * @param bool $filters Flag if the filters should be loaded.
     * @param stdClass $data A database record for the rule.
     * @return void
     */
    public function load($filters = true, $data = null) {
        // Load the rule data.
        if (!$this->loaded && is_null($data)) {
            $this->load_from_record();
        } else if (!$this->loaded) {
            $this->load_from_data($data);
        }
        // Load filters.
        if ($filters && !$this->filtersloaded) {
            $this->load_filters();
            $this->filtersloaded = true;
        }
        $this->loaded = true;
    }

    /**
     * Load all the filters in the database for the rule.
     *
     * @return void
     */
    protected function load_filters() {
        $this->filters = filter::get_filters($this);
    }

    /**
     * Set the data for the rule based on a record passed to it.
     * This is a good way to bulk load data for rules, so only a single
     * database call is made, see the get_all_rules method.
     *
     * @param stdClass $rule
     * @throws \coding_exception
     */
    protected function load_from_data($rule) {
        if ((int)$rule->id !== $this->id) {
            throw new \coding_exception('Record does not match the rule id.');
        }
        try {
            $this->context = \context::instance_by_id($rule->contextid);
        } catch (\Exception $e) {
            // The context is invalid.
            throw new invalid_rule();
        }
        $this->description = $rule->rule_desc;
        $this->role = $rule->roleid;
        $this->title = $rule->rule_name;
    }

    /**
     * Get the rule from the database.
     *
     * @global \moodle_database $DB Moodle database connection object.
     */
    protected function load_from_record() {
        global $DB;
        try {
            $rule = $DB->get_record('local_bulkroleassign_rules', array('id' => $this->id), '*', MUST_EXIST);
            $this->context = \context::instance_by_id($rule->contextid);
        } catch (\Exception $e) {
            // The context is invalid, or the rule has been deleted.
            throw new invalid_rule();
        }
        $this->description = $rule->rule_desc;
        $this->role = $rule->roleid;
        $this->title = $rule->rule_name;
    }

    /**
     * Resets static caches. Should only be used by unit tests.
     *
     * @return void
     */
    public static function reset() {
        self::$roles = null;
        // All the filter types should also be reset.
        filter_user::reset();
        filter_user_info_data::reset();
    }

    /**
     * Runs the filter and assigns any matching users to the category with the defined role.
     * It will also remove the users who no longer meet the criteria.
     *
     * @return void
     */
    public function run() {
        if (!$this->is_valid()) {
            throw new invalid_rule();
        }
        // Get all the users that should be added.
        $newusers = $this->get_new_users();
        foreach ($newusers as $newuser) {
            role_assign($this->role, $newuser->id, $this->context->id, 'local_bulkroleassign', $this->id);
        }
        // Get all the users that should be removed.
        $oldusers = $this->get_old_users();
        foreach ($oldusers as $olduser) {
            role_unassign($olduser->roleid, $olduser->userid, $olduser->contextid, 'local_bulkroleassign', $this->id);
        }
    }

    /**
     * Saves the rule to the database.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return void
     */
    public function save() {
        global $DB;
        if ($this->loaded && !isset($this->id)) {
            $this->create();
        } else if ($this->loaded) {
            $this->update();
        }
        if ($this->filtersloaded) {
            $filterids = array();
            foreach ($this->filters as $filter) {
                $filter->save();
                $filterids[] = $filter->id;
            }
            // We should now delete any filters linked to this
            // rule that we did not just save.
            list($in, $params) = $DB->get_in_or_equal($filterids, SQL_PARAMS_NAMED, 'filter', false);
            $sql = "DELETE FROM {local_bulkroleassign_ufilter} WHERE id $in AND ruleid = :ruleid";
            $DB->execute($sql, array_merge($params, array('ruleid' => $this->id)));
        }
    }

    /**
     * Set the context that the rule applies to.
     *
     * @param \context $context
     */
    public function set_context(\context $context) {
        $this->context = $context;
    }

    /**
     * Set the description for the rule.
     *
     * @param string $description
     */
    public function set_description($description) {
        $this->description = $description;
    }

    /**
     * Sets the role that the rule will assign users to.
     *
     * @param int $role
     */
    public function set_role($role) {
        $this->role = clean_param($role, PARAM_INT);
    }

    /**
     * Set the title for the rule.
     *
     * @param string $title
     */
    public function set_title($title) {
        $this->title = $title;
    }

    /**
     * Updates the rule.
     *
     * @global \moodle_database $DB The Moodle database connection object.
     * @return void
     */
    protected function update() {
        global $DB;
        $rulerecord = (object)array(
            'id' => $this->id,
            'rule_name' => $this->title,
            'rule_desc' => $this->description,
            'roleid' => $this->role,
            'contextid' => $this->context->id,
        );
        $DB->update_record('local_bulkroleassign_rules', $rulerecord);
    }

    /**
     * Gets the SQL used to count the users that will be selected.
     *
     * @return array contains the sql as the first parameter and the parameters for it in the second.
     */
    protected function user_count_sql() {
        list($select, $sqlbody, $params) = $this->base_sql();
        $sql = "SELECT COUNT(*) $sqlbody";
        return array($sql, $params);
    }

    /**
     * Gets the SQL that will find new users that should be added by the rule.
     *
     * @return array contains the sql as the first parameter and the parameters for it in the second.
     */
    protected function user_new_sql() {
        list($select, $sqlbody, $params) = $this->base_sql();
        // Add additional paramteters.
        $params['contextid'] = $this->context->id;
        $params['component'] = 'local_bulkroleassign';
        $params['itemid'] = $this->id;
        $params['roleid'] = $this->role;
        // Build the subselct used to find the ids of users that have already been assigned by the rule.
        $assignselect = "r.userid";
        $assignfrom = "{role_assignments} r";
        $assignwhere = "r.contextid = :contextid AND component = :component AND itemid = :itemid AND roleid = :roleid";
        $assignsql = "SELECT $assignselect FROM $assignfrom WHERE $assignwhere";
        // Build the full query.
        $sql = "$select $sqlbody AND u.id NOT IN ($assignsql) ORDER BY u.lastname, u.firstname";
        return array($sql, $params);
    }

    /**
     * Gets the SQL that finds users who no longer match the rule.
     *
     * @return array contains the sql as the first parameter and the parameters for it in the second.
     */
    protected function user_old_sql() {
        list($select, $sqlbody, $params) = $this->base_sql();
        // Add additional paramteters.
        $params['contextid'] = $this->context->id;
        $params['contextid2'] = $this->context->id;
        $params['component'] = 'local_bulkroleassign';
        $params['itemid'] = $this->id;
        $params['roleid'] = $this->role;
        $userselect = "SELECT u.id";
        $assignselect = "r.*";
        $assignfrom = "{user} u JOIN {role_assignments} r ON u.id = r.userid";
        // Users that are in the current context and not in the selection list,
        // or simply users assigned by this rule to a different context or role.
        $assignwhere = "((r.contextid = :contextid AND r.userid NOT IN ($userselect $sqlbody)) "
                . "OR r.contextid <> :contextid2 OR r.roleid <> :roleid) "
                . "AND component = :component AND itemid = :itemid";
        $sql = "SELECT $assignselect FROM $assignfrom WHERE $assignwhere ORDER BY u.lastname, u.firstname";
        return array($sql, $params);
    }

    /**
     * Get the SQL used to select users.
     *
     * @return array contains the sql as the first parameter and the parameters for it in the second.
     */
    protected function user_select_sql() {
        list($select, $sqlbody, $params) = $this->base_sql();
        $sql = "$select $sqlbody ORDER BY u.lastname, u.firstname";
        return array($sql, $params);
    }
}
