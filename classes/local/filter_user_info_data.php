<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

namespace local_bulkroleassign\local;

defined('MOODLE_INTERNAL') || die();

/**
 * A filter for a custom user profile field.
 *
 * @package    local_bulkroleassign
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @copyright  2017 University of Nottingham
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class filter_user_info_data extends filter implements filter_active {
    /** @var array An array of all the valid types. */
    protected static $types;

    /**
     * @see \local_bulkroleassign\local\filter::__construct()
     */
    public function __construct(rule $rule, $field, $value, $method, $id = null) {
        parent::__construct($rule, $field, $value, $method, $id);
        $this->type = filter::TYPE_CUSTOM;
    }

    /**
     * @see \local_bulkroleassign\local\filter_active::get_valid_types()
     */
    public static function get_valid_types() {
        global $DB;
        if (!isset(self::$types)) {
            self::$types = array();
            $userinfo = $DB->get_records('user_info_field', null, 'name', 'id, name');
            foreach ($userinfo as $info) {
                self::$types[$info->id] = $info->name;
            }
        }
        return self::$types;
    }

    /**
     * @see \local_bulkroleassign\local\filter_active
     */
    public static function reset() {
        self::$types = null;
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_begins()
     */
    protected function sql_begins() {
        global $DB;
        $alias = $this->get_alias();
        $where = $DB->sql_like("$alias.data", ":data$alias");
        $params = array(
            "data$alias" => "$this->value%",
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_contains()
     */
    protected function sql_contains() {
        global $DB;
        $alias = $this->get_alias();
        $where = $DB->sql_like("$alias.data", ":data$alias");
        $params = array(
            "data$alias" => "%$this->value%",
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_equals()
     */
    protected function sql_equals() {
        $alias = $this->get_alias();
        $where = "$alias.data = :data$alias";
        $params = array(
            "data$alias" => $this->value,
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_ends()
     */
    protected function sql_ends() {
        global $DB;
        $alias = $this->get_alias();
        $where = $DB->sql_like("$alias.data", ":data$alias");
        $params = array(
            "data$alias" => "%$this->value",
        );
        return array($where, $params);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_table_name()
     */
    protected function sql_table_name() {
        $tablename = '{user_info_data}';
        // Use the filter id to ensure that the alias for the table is unique.
        $alias = $this->get_alias();
        // The user table will always be present and aliased as u in the rule.
        $on = "u.id = $alias.userid";
        return array($tablename, $on);
    }

    /**
     * @see \local_bulkroleassign\local\filter::sql_where()
     */
    protected function sql_where() {
        list($matchwhere, $matchparams) = parent::sql_where();
        $alias = $this->get_alias();
        // Rows that identify as the specific profile field type need to be selected.
        $where = "($alias.fieldid = :userid$alias AND $matchwhere)";
        $params = array(
            "userid$alias" => $this->field,
        );
        return array($where, array_merge($params, $matchparams));
    }
}
