<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * English language strings.
 *
 * @package    local_bulkroleassign
 * @copyright  2012 - 2017 University of Nottingham
 * @author     Benjamin Ellis <benjamin.ellis@nottingham.ac.uk>
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
$string['addcorefilter'] = ' Add user profile filter';
$string['addcustomfilter'] = 'Add custom profile field filter';
$string['clone'] = 'Clone';
$string['clonerule'] = 'Clone rule';
$string['createrule'] = 'Create rule';
$string['crontask'] = 'Bulk role assignment cron job';
$string['corefilters'] = 'User profile filters';
$string['customfilters'] = 'Custom profile field filters';
$string['deleterule'] = 'Delete';
$string['deleteruletitle'] = 'Delete: {$a->title}';
$string['deleteassignments'] = 'Delete assignments';
$string['confirmdeletemessage'] = 'This action will delete the rule and all assignments attached to it.

Are you sure you wish to continue?';
$string['editrule'] = 'Edit';
$string['invalidrole'] = 'Invalid role';
$string['invalidrule'] = 'The rule is invalid, so will not run';
$string['method_begins'] = 'begins with';
$string['method_contains'] = 'contains';
$string['method_equals'] = 'equals';
$string['method_ends'] = 'ends with';
$string['minfilters'] = 'You must specify at least one core or custom filter';
$string['norules'] = 'There are no rules currently set up';
$string['pluginname'] = 'Bulk role assign';
$string['pluginname_desc'] = 'Plugin to allow role assigments based on a user filter.';
$string['preview'] = 'Preview';
$string['previewtitle'] = 'Preview: {$a->title}';
$string['rulecategory'] = 'Rule category';
$string['rulecategory_help'] = 'The category that users selected by the rule will be given the selected role on';
$string['ruledescription'] = 'Rule description';
$string['ruledescription_help'] = 'Describes the purpose of the rule';
$string['rulerole'] = 'Rule role';
$string['rulerole_help'] = 'The role that users selected by the rule will be assigned';
$string['ruletitle'] = 'Rule title';
$string['ruletitle_help'] = 'The title of a rule must be unique';
$string['run'] = 'Run';
$string['runrule'] = 'Run rule: {$a->title}';
$string['runtaskqueued'] = 'The rule has been queued to run. It will be executed by cron as soon as possible.';
$string['runtaskfailed'] = 'The rule is invalid.

Please change it to be valid, then try again.';
$string['savefail'] = 'The rule failed to save. Please ensure you have a unique Rule title';
$string['saverule'] = 'Save rule';
$string['selectcorefilters'] = 'Select a user profile filter';
$string['selectcustomfilters'] = 'Select a custom profile field filter';
$string['usercount'] = 'Number of users';
