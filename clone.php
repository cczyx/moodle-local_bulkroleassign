<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Page for cloning a rule.
 *
 * @package    local_bulkroleassign
 * @copyright  2017 University of Nottingham
 * @author     Neill Magill <neill.magill@nottingham.ac.uk>
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once(dirname(dirname(__DIR__)) . '/config.php');

use local_bulkroleassign\local\cloneform;

require_login();
require_capability('moodle/site:config', context_system::instance());

$ruleid = required_param('id', PARAM_INT);

$PAGE->set_context(context_system::instance());
$pageurl = new moodle_url('/local/bulkroleassign/clone.php', array('id' => $ruleid));
$pagetitle = get_string('clonerule', 'local_bulkroleassign');
$PAGE->set_url($pageurl);
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('admin');
$PAGE->set_title($pagetitle);
// Add the page to the navigation so that there will be a bread crumb.
$bulkrolenavigation = $PAGE->settingsnav->find('local_bulkroleassign', navigation_node::TYPE_UNKNOWN);
$bulkrolenavigation->add($pagetitle, $pageurl)->make_active();

$formdata = array(
    'rule' => $ruleid,
);
$form = new cloneform(null, $formdata);
// Stores if an expected type of save failure happened.
$savefailed = false;

if ($form->is_cancelled()) {
    redirect(new moodle_url('/local/bulkroleassign/index.php'));
} else if ($form->is_submitted() && $form->is_validated()) {
    try {
        $form->save();
        redirect(new moodle_url('/local/bulkroleassign/index.php'));
    } catch (dml_write_exception $e) {
        // We assume that this is a duplicate title.
        $savefailed = true;
    }
}

// Display the clone form.
$output = $PAGE->get_renderer('local_bulkroleassign');
echo $output->header();
echo $output->heading($pagetitle, 2, 'main');
if ($savefailed) {
    echo $output->notification(get_string('savefail', 'local_bulkroleassign'), 'error');
}
$form->display();
echo $output->footer();

