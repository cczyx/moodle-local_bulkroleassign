<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Bulk Role Assignments Main
 *
 * Plugin to allow role assigments based on a user defined user data filter
 *
 * @package    local_bulkroleassign
 * @copyright  2012 and later Nottingham University
 * @author     Benjamin Ellis - benjamin.ellis@nottingham.ac.uk
 * @license    http:// www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @throws $moodle_exception
 */

require_once(dirname(dirname(__DIR__)) . '/config.php');

use \local_bulkroleassign\local\rule;
use \local_bulkroleassign\output\index;

require_login();
require_capability('moodle/site:config', context_system::instance());

$PAGE->set_context(context_system::instance());
$PAGE->set_url(new moodle_url('/local/bulkroleassign/index.php'));
$PAGE->set_heading($SITE->fullname);
$PAGE->set_pagelayout('admin');
$PAGE->set_title(get_string('pluginname', 'local_bulkroleassign'));

// Display a listy of rules.
$output = $PAGE->get_renderer('local_bulkroleassign');
echo $output->header();
echo $output->heading(get_string('pluginname', 'local_bulkroleassign'), 2, 'main');
$rules = rule::get_all_rules();
$renderable = new index($rules);
echo $output->render($renderable);
echo $output->footer();
