<?php
// This file is part of the bulkroleassgin plugin in Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use local_bulkroleassign\local\rule;
use local_bulkroleassign\local\filter;
use local_bulkroleassign\local\filter_user;

/**
 * Test the methods of the \local_bulkroleassign\local\rule class.
 *
 * @package     local_bulkroleassign
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group local_bulkroleassign
 * @group uon
 */
class local_bulkroleassign_rule_test extends advanced_testcase {
    /**
     * @see \TestCase::tearDown
     */
    public function tearDown() {
        rule::reset();
        parent::tearDown();
    }

    /**
     * Tests that cloning a rule works correctly.
     *
     * @covers \local_bulkroleassign\local\rule::clone_rule
     * @covers \local_bulkroleassign\local\filter_user::clone_filter
     * @covers \local_bulkroleassign\local\filter_user_info_data::clone_filter
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_clone_rule() {
        global $DB;
        $this->resetAfterTest(true);
        // Setup the test.
        $category = self::getDataGenerator()->create_category();
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $rule1 = $generator->create_rule();
        $filter1 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE));
        $filter2 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CUSTOM));
        // Clone the rule.
        $rule = new rule($rule1->id);
        $rule->load();
        $rule->title = 'New rule';
        $rule->clone_rule();
        // The id of the rule record should be different.
        $this->assertNotEquals($rule1->id, $rule->id);
        $rulerecords = $DB->get_records('local_bulkroleassign_rules');
        $this->assertArrayHasKey($rule1->id, $rulerecords);
        $this->assertArrayHasKey($rule->id, $rulerecords);
        // Check that the rule records have similar values.
        $rule1copy = clone($rulerecords[$rule1->id]);
        $rulecopy = clone($rulerecords[$rule->id]);
        unset($rule1copy->id);
        unset($rule1copy->rule_name);
        unset($rulecopy->id);
        unset($rulecopy->rule_name);
        $this->assertEquals($rule1copy, $rulecopy);
        // Test that the filters copied over.
        $rule1filters = $DB->get_records('local_bulkroleassign_ufilter', array('ruleid' => $rule1->id));
        $this->assertCount(2, $rule1filters);
        $rulefilters = $DB->get_records('local_bulkroleassign_ufilter', array('ruleid' => $rule->id));
        $this->assertCount(2, $rulefilters);
        $cleanfilters1 = array();
        foreach ($rule1filters as $filter) {
            unset($filter->id);
            unset($filter->ruleid);
            $cleanfilters1[] = $filter;
        }
        $cleanfilters = array();
        foreach ($rulefilters as $key => $filter) {
            unset($filter->id);
            unset($filter->ruleid);
            $cleanfilters[] = $filter;
        }
        $this->assertEquals($cleanfilters, $cleanfilters1);
    }

    /**
     * Test that data loading on the rules works correctly.
     *
     * @covers local_bulkroleassign\local\rule::__constructor
     * @covers local_bulkroleassign\local\rule::load
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_dataloading() {
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $rule1 = $generator->create_rule();
        $rule2 = $generator->create_rule();
        $filter1 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE));
        $filter2 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CUSTOM));
        $filter3 = $generator->create_filter(array('ruleid' => $rule2->id, 'fldtype' => filter::TYPE_CORE));
        // Create the rule. No data will have been loaded from the database yet.
        $rule = new rule($rule1->id);
        $this->assertFalse($rule->loaded);
        $this->assertFalse($rule->filtersloaded);
        // Getting the id should not load data.
        $this->assertEquals($rule1->id, $rule->id);
        $this->assertFalse($rule->loaded);
        $this->assertFalse($rule->filtersloaded);
        // Now check that we can get the main rule fields. This will not yet load the filters.
        $this->assertEquals($rule1->rule_name, $rule->title);
        $this->assertTrue($rule->loaded);
        $this->assertFalse($rule->filtersloaded);
        $this->assertEquals($rule1->rule_desc, $rule->description);
        $this->assertEquals($rule1->roleid, $rule->role);
        // The rule should load the context of the category.
        $this->assertInstanceOf('context_coursecat', $rule->context);
        $this->assertEquals($rule1->contextid, $rule->context->id);
        // The filters should still not be loaded.
        $this->assertTrue($rule->loaded);
        $this->assertFalse($rule->filtersloaded);
        // Load the filters.
        $filters = $rule->filters;
        $this->assertTrue($rule->loaded);
        $this->assertTrue($rule->filtersloaded);
        // Test that the correct filters were loaded.
        $this->assertCount(2, $filters);
        $this->assertArrayHasKey($filter1->id, $filters);
        $this->assertArrayHasKey($filter2->id, $filters);
        // We will test that the correct object type is loaded,
        // but that filter objects are created and load data correctly is tested else where.
        $this->assertInstanceOf('\local_bulkroleassign\local\filter', $filters[$filter1->id]);
        $this->assertInstanceOf('\local_bulkroleassign\local\filter', $filters[$filter2->id]);
    }

    /**
     * Test deleting a rule works correctly.
     *
     * @covers \local_bulkroleassign\local\rule::delete
     * @covers \local_bulkroleassign\local\rule::delete_filters
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_delete() {
        global $DB;
        $this->resetAfterTest(true);
        // Setup the test.
        $category1 = self::getDataGenerator()->create_category();
        $user1 = self::getDataGenerator()->create_user();
        $context1 = context_coursecat::instance($category1->id);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $rule1 = $generator->create_rule();
        $rule2 = $generator->create_rule();
        $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE));
        $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CUSTOM));
        $generator->create_filter(array('ruleid' => $rule2->id, 'fldtype' => filter::TYPE_CORE));
        // Create some assignments for the rules.
        $generator->create_roleassign(array('userid' => $user1->id, 'itemid' => $rule1->id, 'contextid' => $context1->id));
        $generator->create_roleassign(array('userid' => $user1->id, 'itemid' => $rule2->id, 'contextid' => $context1->id));

        $rule = new rule($rule2->id);
        $rule->delete();
        // Test rule 1 is still present.
        $this->assertTrue($DB->record_exists('local_bulkroleassign_rules', array('id' => $rule1->id)));
        $this->assertEquals(2, $DB->count_records('local_bulkroleassign_ufilter', array('ruleid' => $rule1->id)));
        $this->assertTrue($DB->record_exists('role_assignments', array('component' => 'local_bulkroleassign', 'itemid' => $rule1->id)));
        // Test rule 2 has been deleted.
        $this->assertFalse($DB->record_exists('local_bulkroleassign_rules', array('id' => $rule2->id)));
        $this->assertFalse($DB->record_exists('local_bulkroleassign_ufilter', array('ruleid' => $rule2->id)));
        $this->assertFalse($DB->record_exists('role_assignments', array('component' => 'local_bulkroleassign', 'itemid' => $rule2->id)));
        // Need to test role assignments are deleted.
    }

    /**
     * Test that we can delete all rules on a category.
     *
     * @covers \local_bulkroleassign\local\rule::delete_rules_for_category
     * @covers \local_bulkroleassign\local\rule::delete_filters
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_delete_rules_for_category() {
        global $DB;
        $this->resetAfterTest(true);
        // Setup the test.
        $category1 = self::getDataGenerator()->create_category();
        $category2 = self::getDataGenerator()->create_category();
        $user1 = self::getDataGenerator()->create_user();
        $user2 = self::getDataGenerator()->create_user();
        $context1 = context_coursecat::instance($category1->id);
        $context2 = context_coursecat::instance($category2->id);
        // Create some rules.
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $rule1 = $generator->create_rule(array('contextid' => $context1->id));
        $rule2 = $generator->create_rule(array('contextid' => $context2->id));
        $rule3 = $generator->create_rule(array('contextid' => $context2->id));
        $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE));
        $generator->create_filter(array('ruleid' => $rule2->id, 'fldtype' => filter::TYPE_CUSTOM));
        $generator->create_filter(array('ruleid' => $rule3->id, 'fldtype' => filter::TYPE_CORE));
        // Create some assignments for the rules.
        $generator->create_roleassign(array('userid' => $user1->id, 'itemid' => $rule1->id, 'contextid' => $context1->id));
        $generator->create_roleassign(array('userid' => $user1->id, 'itemid' => $rule2->id, 'contextid' => $context2->id));
        $generator->create_roleassign(array('userid' => $user1->id, 'itemid' => $rule3->id, 'contextid' => $context2->id));
        $generator->create_roleassign(array('userid' => $user2->id, 'itemid' => $rule3->id, 'contextid' => $context2->id));
        // Do the delete.
        rule::delete_rules_for_category($category2->id);
        // Test that the rules we expect are no longer present.
        $this->assertFalse($DB->record_exists('local_bulkroleassign_rules', array('contextid' => $context2->id)));
        $this->assertFalse($DB->record_exists('local_bulkroleassign_ufilter', array('ruleid' => $rule2->id)));
        $this->assertFalse($DB->record_exists('local_bulkroleassign_ufilter', array('ruleid' => $rule3->id)));
        $this->assertFalse($DB->record_exists('role_assignments', array('component' => 'local_bulkroleassign', 'itemid' => $rule2->id)));
        $this->assertFalse($DB->record_exists('role_assignments', array('component' => 'local_bulkroleassign', 'itemid' => $rule3->id)));
        // Test that rule 1 has not been affected.
        $this->assertTrue($DB->record_exists('local_bulkroleassign_rules', array('id' => $rule1->id)));
        $this->assertTrue($DB->record_exists('local_bulkroleassign_ufilter', array('ruleid' => $rule1->id)));
        $this->assertTrue($DB->record_exists('role_assignments', array('component' => 'local_bulkroleassign', 'itemid' => $rule1->id)));
    }

    /**
     * The id of a rule should not be modifiable directly.
     *
     * @covers \local_bulkroleassign\local\rule::__set
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_id_not_modifiable() {
        $rule = new rule();
        $this->expectException('coding_exception');
        $this->expectExceptionMessage('Trying to set invalid property, no set_id() method exists.');
        $rule->id = 6;
    }

    /**
     * Tests that a valid rule is seen as such.
     *
     * @covers \local_bulkroleassign\local\rule::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_vaild_true() {
        global $DB;
        $rule = new rule();
        $rule->title = 'A Title';
        $rule->description = 'A Description';
        $rule->role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $rule->context = context_coursecat::instance($misccategory);
        $filter = new filter_user($rule, 'city', 'Nottingham', filter::METHOD_EQUALS);
        $rule->add_filter($filter);
        $this->assertTrue($rule->is_valid());
    }

    /**
     * Tests that a rule with no filter is seen as invalid.
     *
     * @covers \local_bulkroleassign\local\rule::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_vaild_no_filter() {
        global $DB;
        $rule = new rule();
        $rule->title = 'A Title';
        $rule->description = 'A Description';
        $rule->role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $rule->context = context_coursecat::instance($misccategory);
        $this->assertFalse($rule->is_valid());
    }

    /**
     * Tests that a rule with no title is seen as invalid.
     *
     * @covers \local_bulkroleassign\local\rule::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_valid_no_title() {
        global $DB;
        $rule = new rule();
        $rule->description = 'A Description';
        $rule->role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $rule->context = context_coursecat::instance($misccategory);
        $filter = new filter_user($rule, 'city', 'Nottingham', filter::METHOD_EQUALS);
        $rule->add_filter($filter);
        $this->assertFalse($rule->is_valid());
    }

    /**
     * Tests that a rule with no description is seen as invalid.
     *
     * @covers \local_bulkroleassign\local\rule::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_valid_no_description() {
        global $DB;
        $rule = new rule();
        $rule->title = 'A Title';
        $rule->role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $rule->context = context_coursecat::instance($misccategory);
        $filter = new filter_user($rule, 'city', 'Nottingham', filter::METHOD_EQUALS);
        $rule->add_filter($filter);
        $this->assertFalse($rule->is_valid());
    }

    /**
     * Tests that a rule with an invalid role is seen as invalid.
     *
     * @covers \local_bulkroleassign\local\rule::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_valid_invalid_role() {
        global $DB;
        $rule = new rule();
        $rule->title = 'A Title';
        $rule->description = 'A Description';
        // There will be no negative role ids ever.
        $rule->role = -1;
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $rule->context = context_coursecat::instance($misccategory);
        $filter = new filter_user($rule, 'city', 'Nottingham', filter::METHOD_EQUALS);
        $rule->add_filter($filter);
        $this->assertFalse($rule->is_valid());
    }

    /**
     * Tests that a rule with no context is seen as invalid.
     *
     * @covers \local_bulkroleassign\local\rule::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_valid_no_context() {
        global $DB;
        $rule = new rule();
        $rule->title = 'A Title';
        $rule->description = 'A Description';
        // There will be no negative role ids ever.
        $rule->role = -1;
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $filter = new filter_user($rule, 'city', 'Nottingham', filter::METHOD_EQUALS);
        $rule->add_filter($filter);
        $this->assertFalse($rule->is_valid());
    }

    /**
     * Test that creating a new rule object works correctly.
     *
     * This test does not include saving data.
     *
     * @covers \local_bulkroleassign\local\rule::__constructor
     * @covers \local_bulkroleassign\local\rule::__set
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_newrule() {
        global $DB;
        $title = 'Test rule 1';
        $description = 'Description for a rule.';
        $role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $context = context_coursecat::instance($misccategory);
        // When no id is passed to the constructor it is assumed to be a new rule.
        $rule = new rule();
        $this->assertTrue($rule->loaded);
        $this->assertTrue($rule->filtersloaded);
        // The values should all initially be null.
        $this->assertNull($rule->id);
        $this->assertNull($rule->title);
        $this->assertNull($rule->description);
        $this->assertNull($rule->role);
        $this->assertNull($rule->context);
        $this->assertEmpty($rule->filters);
        // We should be able to set various values.
        $rule->title = $title;
        $rule->description = $description;
        $rule->role = $role;
        $rule->context = $context;
        $this->assertEquals($title, $rule->title);
        $this->assertEquals($description, $rule->description);
        $this->assertEquals($role, $rule->role);
        $this->assertEquals($context, $rule->context);
    }
    
    /**
     * Test saving a rule to the database.
     *
     * @covers \local_bulkroleassign\local\rule::save
     * @covers \local_bulkroleassign\local\rule::create
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_save_new() {
        global $DB;
        $this->resetAfterTest(true);
        // Values we are going to set.
        $title = 'Test rule 1';
        $description = 'Description for a rule.';
        $role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $context = context_coursecat::instance($misccategory);
        // Set up the rule.
        $rule = new rule();
        $rule->title = $title;
        $rule->description = $description;
        $rule->role = $role;
        $rule->context = $context;
        // Add a filter to the rule.
        $filter = new filter_user($rule, 'auth', 'manual', filter::METHOD_CONTAINS);
        $rule->add_filter($filter);
        // Do the save.
        $this->assertNull($rule->id);
        $this->assertNull($filter->id);
        $rule->save();
        $this->assertNotNull($rule->id);
        $this->assertNotNull($filter->id);
        $rulerecord = $DB->get_record('local_bulkroleassign_rules', array('id' => $rule->id), '*', MUST_EXIST);
        $this->assertEquals($rule->title, $rulerecord->rule_name);
        $this->assertEquals($rule->description, $rulerecord->rule_desc);
        $this->assertEquals($rule->role, $rulerecord->roleid);
        $this->assertEquals($rule->context->id, $rulerecord->contextid);
        // Check that the filter saved.
        $this->assertCount(1, $rule->filters);
        $filters = $rule->filters;
        $rulefilter = array_pop($filters);
        $this->assertSame($filter, $rulefilter);
        $this->assertTrue($DB->record_exists('local_bulkroleassign_ufilter', array('id' => $filter->id, 'ruleid' => $rule->id)));
    }

    /**
     * Test updating a rule to the database.
     *
     * @covers \local_bulkroleassign\local\rule::save
     * @covers \local_bulkroleassign\local\rule::update
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_save_update() {
        global $DB;
        $this->resetAfterTest(true);
        // Setup the test.
        $category = self::getDataGenerator()->create_category();
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $rule1 = $generator->create_rule();
        $rule2 = $generator->create_rule();
        $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE));
        $filter3 = $generator->create_filter(array('ruleid' => $rule2->id, 'fldtype' => filter::TYPE_CORE));

        // Values we are going to set.
        $title = 'Test rule 1';
        $description = 'Description for a rule.';
        $role = $DB->get_field('role', 'id', array('shortname' => 'coursecreator'));
        $context = context_coursecat::instance($category->id);

        // Load the rule.
        $rule = new rule($rule2->id);
        // Load data into the rule.
        $rule->load();
        $rule->title = $title;
        $rule->description = $description;
        $rule->role = $role;
        $rule->context = $context;
        // Add an aditional filter.
        $filter = new filter_user($rule, 'auth', 'manual', filter::METHOD_CONTAINS);
        $this->assertCount(1, $rule->filters);
        $rule->add_filter($filter);
        // Save the rule.
        $rule->save();
        // Test the save.
        $rulerecord = $DB->get_record('local_bulkroleassign_rules', array('id' => $rule->id), '*', MUST_EXIST);
        // The rule has changed.
        $this->assertNotEquals($rule2, $rulerecord);
        $this->assertEquals($rule2->id, $rulerecord->id);
        // The rule has changed correctly.
        $this->assertEquals($rule->title, $rulerecord->rule_name);
        $this->assertEquals($rule->description, $rulerecord->rule_desc);
        $this->assertEquals($rule->role, $rulerecord->roleid);
        $this->assertEquals($rule->context->id, $rulerecord->contextid);
        // Test the filters, ensure the old filter is there, and the new one present.
        $this->assertCount(2, $rule->filters);
        $filterrecords = $DB->get_records('local_bulkroleassign_ufilter', array('ruleid' => $rule->id));
        $this->assertCount(2, $filterrecords);
        $this->assertArrayHasKey($filter3->id, $filterrecords);
        $this->assertArrayHasKey($filter->id, $filterrecords);
    }
}
