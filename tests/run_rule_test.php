<?php
// This file is part of the bulkroleassgin plugin in Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use local_bulkroleassign\local\rule;
use local_bulkroleassign\local\filter;
use local_bulkroleassign\local\filter_user;

/**
 * Test the the \local_bulkroleassign\local\rule::run method.
 *
 * These tests are designed to test that it adds users and works with
 * changes to rule details, rather than testing all aspects of the filters
 * find the correct users.
 *
 * @package     local_bulkroleassign
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group local_bulkroleassign
 * @group uon
 */
class local_bulkroleassign_run_rule_test extends advanced_testcase {
    /** @var stdClass Stores a test category created for the tests. */
    protected $category;
    /** @var stdClass Stores a filter record that is used by rule 1. */
    protected $filter1;
    /** @var stdClass Stores a rule that should match user 1 and 2. */
    protected $rule1;
    /** @var stdClass Stores a user that should match rule 1. */
    protected $user1;
    /** @var stdClass Stores a user that should match rule 1. */
    protected $user2;
    /** @var stdClass Stores a user that should not match rule 1. */
    protected $user3;

    /**
     * @see \TestCase::setUp
     */
    public function setUp() {
        global $DB;
        parent::setUp();
        $this->resetAfterTest(true);
        $this->category = self::getDataGenerator()->create_category();
        $this->user1 = self::getDataGenerator()->create_user(array('city' => 'Nottingham'));
        $this->user2 = self::getDataGenerator()->create_user(array('city' => 'Nottingham'));
        $this->user3 = self::getDataGenerator()->create_user(array('city' => 'Derby'));
        $context = context_coursecat::instance($this->category->id);
        $role = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $rule1params = array(
            'roleid' => $role,
            'contextid' => $context->id,
        );
        $this->rule1 = $generator->create_rule($rule1params);
        $filter1params = array(
            'ruleid' => $this->rule1->id,
            'fldtype' => filter::TYPE_CORE,
            'userfld' => 'city',
            'method' => filter::METHOD_EQUALS,
            'filter_value' => 'Nottingham',
        );
        $this->filter1 = $generator->create_filter($filter1params);
    }

    /**
     * @see \TestCase::tearDown
     */
    public function tearDown() {
        rule::reset();
        $this->category = null;
        $this->filter1 = null;
        $this->rule1 = null;
        $this->user1 = null;
        $this->user2 = null;
        $this->user3 = null;
        parent::tearDown();
    }

    /**
     * Tests a rule that has had it's context changed is run.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_category_change() {
        global $DB;
        // Assign the matching users to a different role to rule 1.
        $category = self::getDataGenerator()->create_category();
        $context = context_coursecat::instance($category->id);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $assignment1params = array(
            'itemid' => $this->rule1->id,
            'userid' => $this->user1->id,
            'contextid' => $context->id,
            'roleid' => $this->rule1->roleid,
        );
        $generator->create_roleassign($assignment1params);
        $assignment2params = array(
            'itemid' => $this->rule1->id,
            'userid' => $this->user2->id,
            'contextid' => $context->id,
            'roleid' => $this->rule1->roleid,
        );
        $generator->create_roleassign($assignment2params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // Test that the users were assigned to the correct category.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        // There should be two role assignments for the rule.
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 1 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 2 was assigned.
        $params['userid'] = $this->user2->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }

    /**
     * Tests a rule that has had it's filter changed is run.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_filter_change() {
        global $DB;
        // Have a non matching user assigned before the rule is run.
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $assignment1params = array(
            'itemid' => $this->rule1->id,
            'userid' => $this->user3->id,
            'contextid' => $this->rule1->contextid,
            'roleid' => $this->rule1->roleid,
        );
        $generator->create_roleassign($assignment1params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // Test that the users were assigned to the correct category.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        // There should be two role assignments for the rule.
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 1 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 2 was assigned.
        $params['userid'] = $this->user2->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }

    /**
     * Tests that a rule that has never been run will assign users to the correct category.
     * 
     * @covers \local_bulkroleassign\local\rule::run
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_new_rule() {
        global $DB;
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // Test that the users were assigned to the correct category.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        // There should be two role assignments for the rule.
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 1 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 2 was assigned.
        $params['userid'] = $this->user2->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }

    /**
     * Tests when a rule that has previously assigned one user is run
     * when another user matched the criteria.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_new_user() {
        global $DB;
        // Have a user assigned before the rule is run.
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $assignment1params = array(
            'itemid' => $this->rule1->id,
            'userid' => $this->user1->id,
            'contextid' => $this->rule1->contextid,
            'roleid' => $this->rule1->roleid,
        );
        $assignment1 = $generator->create_roleassign($assignment1params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // Test that the users were assigned to the correct category.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        // There should be two role assignments for the rule.
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 2 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user2->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 1 is still assigned with the same record.
        $params['id'] = $assignment1->id;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }

    /**
     * Tests a rule that has had it's role changed is run.
     *
     * @covers \local_bulkroleassign\local\rule::run
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_role_change() {
        global $DB;
        // Assign the matching users to a different category to rule 1.
        $oldrole = $DB->get_field('role', 'id', array('shortname' => 'coursecreator'));
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $assignment1params = array(
            'itemid' => $this->rule1->id,
            'userid' => $this->user1->id,
            'contextid' => $this->rule1->contextid,
            'roleid' => $oldrole,
        );
        $generator->create_roleassign($assignment1params);
        $assignment2params = array(
            'itemid' => $this->rule1->id,
            'userid' => $this->user2->id,
            'contextid' => $this->rule1->contextid,
            'roleid' => $oldrole,
        );
        $generator->create_roleassign($assignment2params);
        // Load and run the rule.
        $rule = new rule($this->rule1->id);
        $rule->run();
        // Test that the users were assigned to the correct category.
        $params = array(
            'component' => 'local_bulkroleassign',
            'itemid' => $this->rule1->id,
        );
        // There should be two role assignments for the rule.
        $this->assertEquals(2, $DB->count_records('role_assignments', $params));
        // Test user 1 was assigned.
        $params['contextid'] = $this->rule1->contextid;
        $params['roleid'] = $this->rule1->roleid;
        $params['userid'] = $this->user1->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
        // Test user 2 was assigned.
        $params['userid'] = $this->user2->id;
        $this->assertTrue($DB->record_exists('role_assignments', $params));
    }
}
