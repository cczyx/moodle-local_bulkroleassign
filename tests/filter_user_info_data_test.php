<?php
// This file is part of the bulkroleassgin plugin in Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

use local_bulkroleassign\local\rule;
use local_bulkroleassign\local\filter;
use local_bulkroleassign\local\filter_user_info_data;

/**
 * Test the methods of the filter_user class.
 *
 * @package     local_bulkroleassign
 * @copyright   University of Nottingham, 2017
 * @author      Neill Magill <neill.magill@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 * @group local_bulkroleassign
 * @group uon
 */
class local_bulkroleassign_filter_user_info_data_test extends advanced_testcase {
    /**
     * @see \TestCase::tearDown
     */
    public function tearDown() {
        rule::reset();
        parent::tearDown();
    }

    /**
     * Tests that saving a new filter works correctly.
     *
     * @covers \local_bulkroleassign\local\filter::save
     * @covers \local_bulkroleassign\local\filter::create
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_save_new() {
        global $DB;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $userinfofield1 = $generator->create_userinfofield();
        // Setup.
        $rule1 = $generator->create_rule();
        // We will create a filter on the rule to ensure it is not modified.
        $filter1 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE));
        // Set the arguments we will pass to the constructor.
        $rule = new rule($rule1->id);
        $field = 'city';
        $value = 'bob3';
        $method = filter::METHOD_CONTAINS;
        $filter = new filter_user_info_data($rule, $field, $value, $method);
        // Count the records before we save.
        $filterrecords = $DB->count_records('local_bulkroleassign_ufilter');
        $this->assertNull($filter->id);
        $filter->save();
        $this->assertNotNull($filter->id);
        $this->assertNotEquals($filter1->id, $filter->id);
        // Test that a new record has been added.
        $filters = $DB->get_records('local_bulkroleassign_ufilter');
        $this->assertCount($filterrecords + 1, $filters);
        $this->assertArrayHasKey($filter1->id, $filters);
        $this->assertArrayHasKey($filter->id, $filters);
        // Check that the new filter saved correctly.
        $this->assertEquals($filter->id, $filters[$filter->id]->id);
        $this->assertEquals($method, $filters[$filter->id]->method);
        $this->assertEquals($rule->id, $filters[$filter->id]->ruleid);
        $this->assertEquals($value, $filters[$filter->id]->filter_value);
        $this->assertEquals($field, $filters[$filter->id]->userfld);
        $this->assertEquals(filter::TYPE_CUSTOM, $filters[$filter->id]->fldtype);
        // Check the existing filter is unchanged.
        $this->assertEquals($filter1, $filters[$filter1->id]);
    }

    /**
     * Tests that saving an existing filter updates the record correctly.
     *
     * @covers \local_bulkroleassign\local\filter::save
     * @covers \local_bulkroleassign\local\filter::update
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_save_update() {
        global $DB;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        // Setup.
        $rule1 = $generator->create_rule();
        // We will attempt to modify filter 2, filter 1 should not be changed.
        $filter1 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE, 'filter_value' => 'hello'));
        $originalvalues = array(
            'ruleid' => $rule1->id, 
            'fldtype' => filter::TYPE_CUSTOM,
            'filter_value' => 'changeme',
            'userfld' => 'auth',
            'method' => filter::METHOD_ENDS,
        );
        $filter2 = $generator->create_filter($originalvalues);
        // Set the arguments we will pass to the constructor.
        $rule = new rule($rule1->id);
        $field = 'city';
        $value = 'bob3';
        $method = filter::METHOD_CONTAINS;
        $filter = new filter_user_info_data($rule, $field, $value, $method, $filter2->id);
        // Count the records before we save.
        $filterrecords = $DB->count_records('local_bulkroleassign_ufilter');
        $filter->save();
        $filters = $DB->get_records('local_bulkroleassign_ufilter');
        $this->assertCount($filterrecords, $filters);
        // Check out filter records exist.
        $this->assertArrayHasKey($filter1->id, $filters);
        $this->assertArrayHasKey($filter2->id, $filters);
        // Check that filter 1 is unchanged.
        $this->assertEquals($filter1, $filters[$filter1->id]);
        // Check filter 2 is changed and that the correct vaules have saved.
        $this->assertNotEquals($filter2, $filters[$filter2->id]);
        $this->assertEquals($method, $filters[$filter->id]->method);
        $this->assertEquals($rule->id, $filters[$filter->id]->ruleid);
        $this->assertEquals($value, $filters[$filter->id]->filter_value);
        $this->assertEquals($field, $filters[$filter->id]->userfld);
        $this->assertEquals(filter::TYPE_CUSTOM, $filters[$filter->id]->fldtype);
    }

    /**
     * Tests that saving a filter marked for deletion causes it to be deleted.
     *
     * @covers \local_bulkroleassign\local\filter::save
     * @covers \local_bulkroleassign\local\filter::delete
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_delete() {
        global $DB;
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        // Setup.
        $rule1 = $generator->create_rule();
        // We will attempt to modify filter 2, filter 1 should not be changed.
        $filter1 = $generator->create_filter(array('ruleid' => $rule1->id, 'fldtype' => filter::TYPE_CORE, 'filter_value' => 'hello'));
        $originalvalues = array(
            'ruleid' => $rule1->id, 
            'fldtype' => filter::TYPE_CUSTOM,
            'filter_value' => 'changeme',
            'userfld' => 'auth',
            'method' => filter::METHOD_ENDS,
        );
        $filter2 = $generator->create_filter($originalvalues);
        // Set the arguments we will pass to the constructor.
        $rule = new rule($rule1->id);
        $filter = new filter_user_info_data($rule, $filter2->userfld, $filter2->filter_value, $filter2->method, $filter2->id);
        // Count the records before we save.
        $filterrecords = $DB->count_records('local_bulkroleassign_ufilter');
        $filter->mark_for_delete();
        $filter->save();
        $filters = $DB->get_records('local_bulkroleassign_ufilter');
        $this->assertCount($filterrecords - 1, $filters);
        // Check that filter 1 is unchanged.
        $this->assertArrayHasKey($filter1->id, $filters);
        $this->assertEquals($filter1, $filters[$filter1->id]);
        // Check filter 2 has been deleted.
        $this->assertArrayNotHasKey($filter2->id, $filters);
    }

    /**
     * Test that the valid types returns an array.
     *
     * @covers \local_bulkroleassign\local\filter_user_info_data::get_valid_types
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_get_valid_types() {
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $userinfofield1 = $generator->create_userinfofield();

        $validtypes = filter_user_info_data::get_valid_types();
        $this->assertInternalType('array', $validtypes);
        $this->assertGreaterThan(0, count($validtypes));
    }

    /**
     * Test that when the filter should be valid it says so.
     *
     * @covers \local_bulkroleassign\local\filter::is_valid
     */
    public function test_is_valid_true() {
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $userinfofield1 = $generator->create_userinfofield();

        $rule = new rule();
        $filter = new filter_user_info_data($rule, $userinfofield1->id, 'manual', filter::METHOD_EQUALS);
        $this->assertTrue($filter->is_valid());
    }

    /**
     * Tests that a filter with an invalid method is not seen as valid.
     *
     * @covers \local_bulkroleassign\local\filter::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_valid_wrong_method() {
        $this->resetAfterTest(true);
        $generator = self::getDataGenerator()->get_plugin_generator('local_bulkroleassign');
        $userinfofield1 = $generator->create_userinfofield();

        $rule = new rule();
        // The method should never be negative.
        $filter = new filter_user_info_data($rule, $userinfofield1->id, 'manual', -1);
        $this->assertFalse($filter->is_valid());
    }

    /**
     * Tests that a filter with an invalid method is not seen as valid.
     *
     * @covers \local_bulkroleassign\local\filter::is_valid
     * @group local_bulkroleassign
     * @group uon
     */
    public function test_is_valid_wrong_field() {
        $rule = new rule();
        // There are no user info fields.
        $filter = new filter_user_info_data($rule, 1, 'manual', filter::METHOD_EQUALS);
        $this->assertFalse($filter->is_valid());
    }
}
