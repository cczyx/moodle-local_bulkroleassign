@local @uon @local_bulkroleassign
Feature: Rule editing
    In order to change rule criteria
    As an admin
    I need to be able to edit a rule.

    Background:
        Given the following "categories" exist:
            | name | category | idnumber |
            | Cat 1 | 0 | CAT1 |
        And the following "users" exist:
            | username | firstname | lastname | email | city |
            | student1 | Sam | Student | student1@example.com | Nottingham |
            | teacher1 | Teacher | One | teacher1@example.com | London |
            | teacher2 | Tutor | Two | teacher2@example.com | Northampton |
            | teacher3 | Lecturer | Three | teacher3@example.com | Nottingham |
        And I create a user info field:
            | shortname | name | datatype | defaultdata |
            | tst | test | text | 0 |
        And I create user info data:
            | username | fieldshortname | data |
            | student1 | tst | 1 |
            | teacher1 | tst | 2 |
            | teacher2 | tst | 1 |
            | teacher3 | tst | 2 |

    Scenario: Add a new filter
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | core | city | equals | Nottingham |
        And I log in as "admin"
        And I navigate to "Users > Permissions > Bulk role assign" in site administration
        When I press "Edit"
        And I set the following "custom" filters on the bulk role assign form:
            | field | condition | filter |
            | tst | equals | 2 |
        And I press "Save"
        And I press "Preview"
        And I should see "Lecturer Three"
        But I should not see "Sam Student"
        And I should not see "Teacher One"
        And I should not see "Tutor Two"

    Scenario: Delete a filter
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | core | city | equals | Nottingham |
            | custom | tst | equals | 2 |
        And I log in as "admin"
        And I navigate to "Users > Permissions > Bulk role assign" in site administration
        When I press "Edit"
        # An empty filter value should delete a rule.
        And I set the following "custom" filters on the bulk role assign form:
            | field | condition | filter |
            | tst | equals | |
        And I press "Save"
        And I press "Preview"
        Then I should see "Sam Student"
        And I should see "Lecturer Three"
        But I should not see "Teacher One"
        And I should not see "Tutor Two"

    Scenario: Modify a filter
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | core | city | equals | Nottingham |
        And I log in as "admin"
        And I navigate to "Users > Permissions > Bulk role assign" in site administration
        When I press "Edit"
        # This will overwrite the first core filter.
        And I set the following "core" filters on the bulk role assign form:
            | field | condition | filter |
            | city | ends | on |
        And I press "Save"
        And I press "Preview"
        And I should see "Teacher One"
        And I should see "Tutor Two"
        But I should not see "Sam Student"
        And I should not see "Lecturer Three"

    @javascript
    Scenario: Edit main properties
        Given the following bulk role assign rules exist:
            | title | description | role | category |
            | Test rule | A rule for testing | manager | CAT1 |
        And the following bulk role assign filters exist for "Test rule":
            | type | field | condition | value |
            | core | city | equals | Nottingham |
        And the following "categories" exist:
            | name | category | idnumber |
            | Cat 2 | 0 | CAT2 |
        And I log in as "admin"
        And I navigate to "Users > Permissions > Bulk role assign" in site administration
        When I press "Edit"
        And I set the bulk role assign form with:
            | title | Different title |
            | description | Description that has changed |
            | role | coursecreator |
            | category | CAT2 |
        And I press "Save rule"
        # Tests that the old rule is not displayed.
        Then I should not see "Test rule"
        And I should not see "A rule for testing"
        And I should not see "Manager"
        And I should not see "Category: Cat 1"
        # Tests that the changed rule is displayed.
        Then I should see "Different title"
        And I should see "Description that has changed"
        And I should see "Course creator"
        And I should see "Category: Cat 2"
