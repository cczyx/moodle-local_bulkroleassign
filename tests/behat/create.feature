@local @uon @local_bulkroleassign @javascript
Feature: Rule creation
    In order to assign users automatically to categories
    As an admin
    I need to be able to create bulk role assign rules.

    Background:
        Given the following "categories" exist:
            | name | category | idnumber |
            | Cat 1 | 0 | CAT1 |
        And the following "users" exist:
            | username | firstname | lastname | email | city |
            | student1 | Sam | Student | student1@example.com | Nottingham |
            | teacher1 | Teacher | One | teacher1@example.com | London |
            | teacher2 | Tutor | Two | teacher2@example.com | Northampton |
            | teacher3 | Lecturer | Three | teacher3@example.com | Nottingham |
        And I create a user info field:
            | shortname | name | datatype | defaultdata |
            | tst | test | text | 0 |
        And I create user info data:
            | username | fieldshortname | data |
            | student1 | tst | 1 |
            | teacher1 | tst | 2 |
            | teacher2 | tst | 1 |
            | teacher3 | tst | 2 |

    Scenario: Custom user profile rule
        Given I log in as "admin"
        And I navigate to "Users > Permissions > Bulk role assign" in site administration
        And I should not see "Test rule"
        And I should not see "A rule for a custom user profile field"
        And I should not see "Manager"
        And I should not see "Category: Cat 1"
        And I press "Create rule"
        Given I set the bulk role assign form with:
            | title | Test rule |
            | description | A rule for a custom user profile field |
            | role | manager |
            | category | CAT1 |
        And I set the following "custom" filters on the bulk role assign form:
            | field | condition | filter |
            | tst | equals | 2 |
        And I press "Save rule"
        # Tests that the rule is displayed.
        Then I should see "Test rule"
        And I should see "A rule for a custom user profile field"
        And I should see "Manager"
        And I should see "Category: Cat 1"
        # Test that the filters have been saved.
        And I press "Preview"
        Then I should see "Teacher One"
        And I should see "Lecturer Three"
        But I should not see "Sam Student"
        And I should not see "Tutor Two"

    Scenario: Core user profile rule
        Given I log in as "admin"
        And I navigate to "Users > Permissions > Bulk role assign" in site administration
        And I should not see "Test rule"
        And I should not see "A rule for a core user profile field"
        And I should not see "Manager"
        And I should not see "Category: Cat 1"
        And I press "Create rule"
        Given I set the bulk role assign form with:
            | title | Test rule |
            | description | A rule for a core user profile field |
            | role | manager |
            | category | CAT1 |
        And I set the following "core" filters on the bulk role assign form:
            | field | condition | filter |
            | city | equals | Nottingham |
        And I press "Save rule"
        Then I should see "Test rule"
        And I should see "A rule for a core user profile field"
        And I should see "Manager"
        And I should see "Category: Cat 1"
        # Test that the filters have been saved.
        And I press "Preview"
        Then I should see "Sam Student"
        And I should see "Lecturer Three"
        But I should not see "Teacher One"
        And I should not see "Tutor Two"

    Scenario: Multiple user profile rule
        Given I log in as "admin"
        And I navigate to "Users > Permissions > Bulk role assign" in site administration
        And I should not see "Test rule"
        And I should not see "A rule for a core and custom user profile field"
        And I should not see "Manager"
        And I should not see "Category: Cat 1"
        And I press "Create rule"
        Given I set the bulk role assign form with:
            | title | Test rule |
            | description | A rule for a core and custom user profile field |
            | role | manager |
            | category | CAT1 |
        And I set the following "core" filters on the bulk role assign form:
            | field | condition | filter |
            | city | equals | Nottingham |
        And I set the following "custom" filters on the bulk role assign form:
            | field | condition | filter |
            | tst | equals | 2 |
        And I press "Save rule"
        Then I should see "Test rule"
        And I should see "A rule for a core and custom user profile field"
        And I should see "Manager"
        And I should see "Category: Cat 1"
        # Test that the filters have been saved.
        And I press "Preview"
        And I should see "Lecturer Three"
        But I should not see "Sam Student"
        And I should not see "Teacher One"
        And I should not see "Tutor Two"
