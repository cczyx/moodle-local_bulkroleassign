<?php
// This file is part of the bulk role assign local plugin for Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.
// NOTE: no MOODLE_INTERNAL test here, this file may be required by behat before including /config.php.

/**
 * Step definitions.
 *
 * @package    local_bulkroleassign
 * @category   test
 * @copyright  2014 onwards, University of Nottingham
 * @author     Joseph Baxter (joseph.baxter@nottingham.ac.uk)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
require_once(__DIR__ . '/../../../../lib/behat/behat_base.php');

use Behat\Gherkin\Node\TableNode as TableNode,
    Behat\Mink\Exception\ExpectationException as ExpectationException,
    Behat\Behat\Tester\Exception\PendingException as PendingException;
use local_bulkroleassign\local\filter;

/**
 * Bulk role assignment steps definitions.
 *
 * @package    local_bulkroleassign
 * @category   test
 * @copyright  2014 University of Nottingham
 * @author     Joseph Baxter (joseph.baxter@nottingham.ac.uk)
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class behat_local_bulkroleassign extends behat_base {
    /**
     * User info field object
     * @var object 
     */
    protected $info;

    /**
     * Creates a new user info field data set
     *
     * @Given /^I create a user info field:$/
     * @param TableNode $infofields record set.
     */
    public function i_create_a_user_info_field(TableNode $infofields) {
        $this->datagenerator = testing_util::get_data_generator()->get_plugin_generator('local_bulkroleassign');

        if ($this->datagenerator) {
            $header = array();
            $record = array();
            foreach ($infofields->getRows() as $infofield) {
                if (count($header) == 0) {
                    $header = array_merge($header, $infofield);
                    $shortnameindex = array_search('shortname', $header);
                    $nameindex = array_search('name', $header);
                    $datatypeindex = array_search('datatype', $header);
                    $defaultdataindex = array_search('defaultdata', $header);
                    continue;
                } else {
                    if (is_int($shortnameindex)) {
                        $record['shortname'] = $infofield[$shortnameindex];
                    }
                    if (is_int($nameindex)) {
                        $record['name'] = $infofield[$nameindex];
                    }
                    if (is_int($datatypeindex)) {
                        $record['datatype'] = $infofield[$datatypeindex];
                    }
                    if (is_int($defaultdataindex)) {
                        $record['defaultdata'] = $infofield[$defaultdataindex];
                    }
                    $this->info = $this->datagenerator->create_userinfofield($record);
                }
            }
        } else {
            throw new PendingException();
        }
    }

    /**
     * Creates a new user info data data set
     *
     * @Given /^I create user info data:$/
     * @global moodle_database $DB
     * @param TableNode $infodatas record set.
     */
    public function i_create_user_info_data(TableNode $infodatas) {
        global $DB;

        $this->datagenerator = testing_util::get_data_generator()->get_plugin_generator('local_bulkroleassign');

        if ($this->datagenerator) {
            $header = array();
            $record = array();
            foreach ($infodatas->getRows() as $infodata) {
                if (count($header) == 0) {
                    $header = array_merge($header, $infodata);
                    $usernameindex = array_search('username', $header);
                    $shortnameindex = array_search('fieldshortname', $header);
                    $dataindex = array_search('data', $header);
                    continue;
                } else {
                    // Get userid.
                    if (is_int($usernameindex)) {
                        $username = $infodata[$usernameindex];
                        $userwhere = array('username' => $username);
                        $record['userid'] = $DB->get_field('user', 'id', $userwhere);
                    }

                    // Get fieldid.
                    if (is_int($shortnameindex)) {
                        $shortname = $infodata[$shortnameindex];
                        $infowhere = array('shortname' => $shortname);
                        $record['fieldid'] = $DB->get_field('user_info_field', 'id', $infowhere);
                    }

                    if (is_int($dataindex)) {
                        $record['data'] = $infodata[$dataindex];
                    }

                    $this->info = $this->datagenerator->create_userinfodata($record);
                }
            }
        } else {
            throw new PendingException();
        }
    }

    /**
     * Fills in the non filter parts of a bulk role assign form. Values that can be passed are:
     * - title: The title of the rule
     * - description: The rule description
     * - role: The shortname of a role
     * - category: The idnumber of a category
     *
     * @Given I set the bulk role assign form with:
     * @global moodle_database $DB
     * @param TableNode $values
     * @return void
     */
    public function i_set_the_bulk_role_assign_form_with(TableNode $values) {
        global $DB;
        $data = $values->getRowsHash();
        $table = array();
        // Get the title passed.
        if (!empty($data['title'])) {
            $table[] = array('id_title', $data['title']);
        }
        // Get the description passed.
        if (!empty($data['description'])) {
            $table[] = array('id_description', $data['description']);
        }
        // Get the role passed.
        if (!empty($data['role'])) {
            $roleid = $DB->get_field('role', 'id', array('shortname' => $data['role']), MUST_EXIST);
            $table[] = array('id_roleid', $roleid);
        }
        // Get the category passed.
        if (!empty($data['category'])) {
            $category = $DB->get_field('course_categories', 'name', array('idnumber' => $data['category']), MUST_EXIST);
        } else {
            $category = $DB->get_field('course_categories', 'name', array('name' => 'Miscellaneous'));
        }
        $table[] = array(get_string('rulecategory', 'local_bulkroleassign'), $category);
        // Fill in the form.
        $this->execute('behat_forms::i_set_the_following_fields_to_these_values', array(new TableNode($table)));
    }

    /**
     * Checks the values in the non filter parts of a bulk role assign form. Values that can be passed are:
     * - title: The title of the rule
     * - description: The rule description
     * - role: The shortname of a role
     * - category: The idnumber of a category
     *
     * @Given the bulk role assign form match these values:
     * @global moodle_database $DB
     * @param TableNode $values
     * @return void
     */
    public function the_bulk_role_assign_form_match_these_values(TableNode $values) {
        global $DB;
        $data = $values->getRowsHash();
        $table = array();
        // Get the title passed.
        if (!empty($data['title'])) {
            $table[] = array('id_title', $data['title']);
        }
        // Get the description passed.
        if (!empty($data['description'])) {
            $table[] = array('id_description', $data['description']);
        }
        // Get the role passed.
        if (!empty($data['role'])) {
            $roleid = $DB->get_field('role', 'id', array('shortname' => $data['role']), MUST_EXIST);
            $table[] = array('id_roleid', $roleid);
        }
        // Get the category passed.
        if (!empty($data['category'])) {
            $category = $DB->get_field('course_categories', 'name', array('idnumber' => $data['category']), MUST_EXIST);
        } else {
            $category = $DB->get_field('course_categories', 'name', array('name' => 'Miscellaneous'));
        }
        $table[] = array(get_string('rulecategory', 'local_bulkroleassign'), $category);
        // Fill in the form.
        $this->execute('behat_forms::the_following_fields_match_these_values', array(new TableNode($table)));
    }

    /**
     * Fills out the filter parts of bulk role assign form. Requires the following values:
     * - field: The shortname of the user profile field to be selected
     * - condition: the way the filter should be used.
     * - filter: the value that should be looked for in the user profile field
     *
     * @Given /^I set the following "([^"]*)" filters on the bulk role assign form:$/
     * @global moodle_database $DB
     * @param string $type The type of filters to set.
     * @param TableNode $filters
     * @return void
     */
    public function i_set_the_following_filters_on_the_bulk_role_assign_form($type, TableNode $filters) {
        global $DB;
        $filternumber = 1;
        $data = array();
        if ($type === 'custom') {
            $fieldprefix = 'customfilter';
        } else {
            // Assume a core profile filter.
            $fieldprefix = 'corefilter';
        }
        foreach ($filters->getHash() as $filter) {
            switch ($fieldprefix) {
                case 'customfilter':
                    $field = $DB->get_field('user_info_field', 'id', array('shortname' => $filter['field']), MUST_EXIST);
                    break;
                case 'corefilter':
                    $field = $filter['field'];
                    break;
            }
            // Validate the method.
            switch ($filter['condition']) {
                case 'begins':
                    $method = filter::METHOD_BEGINS;
                    break;
                case 'contains':
                    $method = filter::METHOD_BEGINS;
                    break;
                case 'ends':
                    $method = filter::METHOD_ENDS;
                    break;
                case 'equals':
                    $method = filter::METHOD_EQUALS;
                    break;
                default:
                    throw new PendingException('Filter condition not implemented');
            }
            $data[] = array("{$fieldprefix}field{$filternumber}", $field);
            $data[] = array("{$fieldprefix}method{$filternumber}", $method);
            $data[] = array("{$fieldprefix}value{$filternumber}", $filter['filter']);
            $filternumber++;
        }
        $this->execute('behat_forms::i_set_the_following_fields_to_these_values', array(new TableNode($data)));
    }

    /**
     * Checks the values in the filter parts of bulk role assign form. Requires the following values:
     * - field: The shortname of the user profile field to be selected
     * - condition: the way the filter should be used.
     * - filter: the value that should be looked for in the user profile field
     *
     * @Given the bulk role assign form filters match these values:
     * @global moodle_database $DB
     * @param TableNode $filters
     * @return void
     */
    public function the_bulk_role_assign_form_filters_match_these_values(TableNode $filters) {
        global $DB;
        $filternumber = 1;
        $data = array();
        foreach ($filters->getHash() as $filter) {
            if (!empty($filter['type']) && $filter['type'] === 'custom') {
                $fieldprefix = 'customfilter';
                $field = $DB->get_field('user_info_field', 'id', array('shortname' => $filter['field']), MUST_EXIST);
            } else {
                // Assume a core profile filter.
                $fieldprefix = 'corefilter';
                $field = $filter['field'];
            }
            // Validate the method.
            switch ($filter['condition']) {
                case 'begins':
                    $method = filter::METHOD_BEGINS;
                    break;
                case 'contains':
                    $method = filter::METHOD_BEGINS;
                    break;
                case 'ends':
                    $method = filter::METHOD_ENDS;
                    break;
                case 'equals':
                    $method = filter::METHOD_EQUALS;
                    break;
                default:
                    throw new PendingException('Filter condition not implemented');
            }
            $data[] = array("{$fieldprefix}field{$filternumber}", $field);
            $data[] = array("{$fieldprefix}method{$filternumber}", $method);
            $data[] = array("{$fieldprefix}value{$filternumber}", $filter['filter']);
            $filternumber++;
        }
        $this->execute('behat_forms::the_following_fields_match_these_values', array(new TableNode($data)));
    }

    /**
     * Creates a bulk role assign rule in the database.
     *
     * @Given the following bulk role assign rules exist:
     * @param TableNode $rules
     * @return void
     */
    public function the_following_bulk_role_assign_rules_exist(TableNode $rules) {
        global $DB;
        $datagenerator = testing_util::get_data_generator()->get_plugin_generator('local_bulkroleassign');
        foreach ($rules->getHash() as $rule) {
            $category = $DB->get_field('course_categories', 'id', array('idnumber' => $rule['category']), MUST_EXIST);
            $role = $DB->get_field('role', 'id', array('shortname' => $rule['role']), MUST_EXIST);
            $record = array(
                'rule_name' => $rule['title'],
                'rule_desc' => $rule['description'],
                'roleid' => $role,
                'contextid' => context_coursecat::instance($category)->id,
            );
            $datagenerator->create_rule($record);
        }
    }

    /**
     * Creates the filters for a bulk role assign rule.
     * 
     * @Given /^the following bulk role assign filters exist for "([^"]*)":$/
     * @global moodle_database $DB
     * @param string $rule Name of the rule the filters should be applied to.
     * @param TableNode $filters
     */
    public function the_following_bulk_role_assign_filters_exist_for_rule($rule, TableNode $filters) {
        global $DB;
        // Rule names are unique so we should find one.
        $ruleid = $DB->get_field('local_bulkroleassign_rules', 'id', array('rule_name' => $rule), MUST_EXIST);
        $datagenerator = testing_util::get_data_generator()->get_plugin_generator('local_bulkroleassign');
        foreach ($filters->getHash() as $filter) {
            // Validate the type, then get the name of the field we need to store.
            if ($filter['type'] === 'custom') {
                $type = filter::TYPE_CUSTOM;
                $field = $DB->get_field('user_info_field', 'id', array('shortname' => $filter['field']), MUST_EXIST);
            } else if ($filter['type'] === 'core') {
                $type = filter::TYPE_CORE;
                $field = $filter['field'];
            } else {
                // Unknown filter type.
                throw new PendingException('Filter type not implemented');
            }
            // Validate the method.
            switch ($filter['condition']) {
                case 'begins':
                    $method = filter::METHOD_BEGINS;
                    break;
                case 'contains':
                    $method = filter::METHOD_BEGINS;
                    break;
                case 'ends':
                    $method = filter::METHOD_ENDS;
                    break;
                case 'equals':
                    $method = filter::METHOD_EQUALS;
                    break;
                default:
                    throw new PendingException('Filter condition not implemented');
            }
            $record = array(
                'ruleid' => $ruleid,
                'fldtype' => $type,
                'userfld' => $field,
                'filter_value' => $filter['value'],
                'method' => $method,
            );
            $datagenerator->create_filter($record);
        }
    }
}
