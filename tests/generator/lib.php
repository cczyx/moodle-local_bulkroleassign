<?php
// This file is part of the bulkroleassign Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * Data generator
 *
 * @package     local_bulkroleassign
 * @copyright   University of Nottingham, 2014 onwards
 * @author      Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
defined('MOODLE_INTERNAL') || die();

use local_bulkroleassign\local\rule;
use local_bulkroleassign\local\filter;
use local_bulkroleassign\local\filter_user;

/**
 * Used to generate data for local_autouploadcourses.
 *
 * @package     local_bulkroleassign
 * @copyright   University of Nottingham, 2014
 * @author      Joseph Baxter <joseph.baxter@nottingham.ac.uk>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */
class local_bulkroleassign_generator extends component_generator_base {
    /** @var int Stores the number of times the generator has been used to generate items that require unique data. */
    protected static $count = 0;

    /**
     * Create a new bulk role assignment rule.
     *
     * @global moodle_database $DB The Moodle database connection object.
     * @param array|stdClass $record
     * @return stdClass bulkroleassign_rules record.
     * @throws coding_exception
     */
    public function create_rule($record = null) {
        global $DB;
        self::$count++;
        // Standardise the record as an array.
        $record = (array)$record;
        // Setup the defaults.
        $managerrole = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));

        $defaults = array(
            'rule_name' => 'test ' . self::$count,
            'rule_desc' => 'Filter description ' . self::$count,
            'roleid' => $managerrole,
            'contextid' => context_coursecat::instance($misccategory)->id,
        );

        $record = $this->datagenerator->combine_defaults_and_record($defaults, $record);
        $record['id'] = $DB->insert_record('local_bulkroleassign_rules', $record);
        return (object) $record;
    }

    /**
     * Create a new bulk role assignment user filters.
     *
     * @global moodle_database $DB The Moodle database connection object.
     * @param array|stdClass $record
     * @return stdClass bulkroleassign_ufilter record.
     * @throws coding_exception
     */
    public function create_filter($record = null) {
        global $DB;
        // Standardise the record as an array.
        $record = (array)$record;
        // Check for the presence of compulsory fields.
        if (empty($record['ruleid'])) {
            throw new coding_exception('ruleid must be passed for rules to function');
        }

        // Get the first core user profile field that can be processed.
        $fieldtypes = filter_user::get_valid_types();
        $fieldkeys = array_keys($fieldtypes);
        $defaultfield = array_shift($fieldkeys);

        $defaults = array(
            'ruleid' => 1,
            'fldtype' => filter::TYPE_CORE,
            'userfld' => $defaultfield,
            'filter_value' => 'manual',
            'method' => filter::METHOD_EQUALS,
        );

        $record = $this->datagenerator->combine_defaults_and_record($defaults, $record);
        $record['id'] = $DB->insert_record('local_bulkroleassign_ufilter', $record);
        return (object) $record;
    }

    /**
     * Create a new role assignment.
     *
     * @global moodle_database $DB The Moodle database connection object.
     * @param array|stdClass $record
     * @return stdClass role_assignments record.
     * @throws coding_exception
     */
    public function create_roleassign($record = null) {
        global $DB;
        // Standardise the record as an array.
        $record = (array)$record;
        // Check for the presence of compulsory fields.
        if (empty($record['itemid']) && !empty($record['ruleid'])) {
            $record['itemid'] = $record['ruleid'];
        } else if (empty($record['itemid'])) {
            throw new coding_exception('itemid or ruleid must be passed');
        }

        if (empty($record['userid'])) {
            throw new coding_exception('userid must be passed');
        }
        // Setup the defaults.
        $managerrole = $DB->get_field('role', 'id', array('shortname' => 'manager'));
        $misccategory = $DB->get_field('course_categories', 'id', array('name' => 'Miscellaneous'));
        $misccontext = context_coursecat::instance($misccategory);

        $defaults = array(
            'roleid' => $managerrole,
            'contextid' => $misccontext->id,
            'userid' => 1,
            'component' => 'local_bulkroleassign',
            'itemid' => 1,
        );

        $record = $this->datagenerator->combine_defaults_and_record($defaults, $record);
        $record['id'] = $DB->insert_record('role_assignments', $record);
        return (object) $record;
    }

    /**
     * Create a new user info field.
     *
     * @global moodle_database $DB The Moodle database connection object.
     * @param array|stdClass $record
     * @return stdClass user_info_field record.
     * @throws coding_exception
     */
    public function create_userinfofield($record = null) {
        global $DB;
        self::$count++;
        // Standardise the record as an array.
        $record = (array)$record;
        // Setup the defaults.
        $defaults = array(
            'shortname' => 'tst' . self::$count,
            'name' => 'test ' . self::$count,
            'datatype' => 'text',
            'defaultdata' => 0
        );

        $record = $this->datagenerator->combine_defaults_and_record($defaults, $record);
        $record['id'] = $DB->insert_record('user_info_field', $record);
        return (object) $record;
    }

    /**
     * Create a new user info data.
     *
     * @global moodle_database $DB The Moodle database connection object.
     * @param array|stdClass $record
     * @return stdClass user_info_data record.
     * @throws coding_exception
     */
    public function create_userinfodata($record = null) {
        global $DB;
        // Standardise the record as an array.
        $record = (array)$record;
        // Check for the presence of compulsory fields.
        if (empty($record['userid'])) {
            throw new coding_exception('userid must be passed');
        }
        if (empty($record['fieldid'])) {
            throw new coding_exception('fieldid must be passed');
        }
        // Setup the defaults.
        $defaults = array(
            'userid' => 1,
            'fieldid' => 1,
            'data' => 1
        );

        $record = $this->datagenerator->combine_defaults_and_record($defaults, $record);
        $record['id'] = $DB->insert_record('user_info_data', $record);
        return (object) $record;
    }

    /**
     * @see \component_generator_base::reset
     */
    public function reset() {
        self::$count = 0;
        parent::reset();
    }
}
